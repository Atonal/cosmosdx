﻿using HAJE.Cosmos.Input;
using HAJE.Cosmos.SystemComponent;
using SharpDX.Direct3D9;
using SharpDX.Windows;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace HAJE.Cosmos
{
    /// <summary>
    /// 게임에 단 하나만 존재 할 수 있는 종류의 것들을 포함하고 있는 정적 객체
    /// 게임 메인 창, 그래픽 장치, 키보드 마우스 장치 등을 다룬다.
    /// </summary>
    public static class GameSystem
    {
        public static RenderForm GameForm
        {
            get;
            private set;
        }

        public static Device GraphicsDevice
        {
            get;
            private set;
        }

        public static BuildInfo BuildInfo
        {
            get;
            private set;
        }

        public static Mouse Mouse
        {
            get;
            private set;
        }

        public static Keyboard Keyboard
        {
            get;
            private set;
        }

        public static LogSystem Log
        {
            get;
            private set;
        }

        public static Profiler Profiler
        {
            get;
            private set;
        }

        public static DebugOutput DebugOutput
        {
            get;
            private set;
        }

        public static UpdateDispatcher UpdateLoop
        {
            get;
            private set;
        }

        public static GameTime GameTime
        {
            get;
            private set;
        }

        #region initialization helpers

        static void InitializeCurrentDirectory()
        {
            // Resource 폴더를 일일이 복사하지 않더라도 실행 경로를 수정. 테스트 가능하도록
            if (!Directory.Exists(Environment.CurrentDirectory + "\\Resource"))
            {
                if (Debugger.IsAttached || Directory.Exists(Environment.CurrentDirectory + "\\..\\..\\Resource"))
                {
                    Environment.CurrentDirectory = "../../";
                }
            }
            if (!Directory.Exists(Environment.CurrentDirectory + "\\Resource"))
            {
                throw new FileNotFoundException("리소스 파일의 경로를 찾지 못했습니다.\n현재 경로: " + Environment.CurrentDirectory + "\n" );
            }
        }

        static void InitializeErrorReporting()
        {
            bool errorReportOnDebugMode = false;
            if (!Debugger.IsAttached || errorReportOnDebugMode)
            {
                Action<Exception> errorHandler = (e) =>
                {
                    MessageBox.Show("처리되지 않은 예외가 발생했습니다.\n" +
                        "게임을 종료합니다.\n\n" +
                        "오류내용 -----------------\n" + e, "오류");
                    File.WriteAllText("error.log", e.ToString());
                    Environment.Exit(-1);
                };
                Application.ThreadException += (o, e) => { errorHandler(e.Exception); };
                Thread.GetDomain().UnhandledException += (o, e) => { errorHandler((Exception)e.ExceptionObject); };
            }
        }

        static RenderForm CreateGameForm()
        {
            var form = new RenderForm("Cosmos Online");
            form.ClientSize = new System.Drawing.Size(1600, 900);
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MaximizeBox = false;
            form.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            return form;
        }

        static Device CreateGraphicsDevice(RenderForm form)
        {
            var pp = new PresentParameters(form.ClientRectangle.Width, form.ClientRectangle.Height);
            pp.Windowed = true;
            pp.PresentationInterval = PresentInterval.Immediate;
            var device = new Device(new Direct3D(),
                0,
                DeviceType.Hardware,
                form.Handle,
                CreateFlags.HardwareVertexProcessing,
                pp);
            if (device.DriverLevel != DriverLevel.Direct3D9)
            {
                throw new NotSupportedException("DirectX 장치를 올바르게 초기화하지 못했습니다.");
            }
            return device;
        }

        #endregion

        public static void Initialize()
        {
            Debug.Assert(initialized == false);

            BuildInfo = new SystemComponent.BuildInfo();
            InitializeErrorReporting();
            InitializeCurrentDirectory();
            GameForm = CreateGameForm();
            GraphicsDevice = CreateGraphicsDevice(GameForm);
            Mouse = new Input.Mouse(GameForm);
            Keyboard = new Input.Keyboard(GameForm);
            Log = new LogSystem();
            Profiler = new SystemComponent.Profiler();
            DebugOutput = new DebugOutput(GraphicsDevice, Keyboard);
            UpdateLoop = new UpdateDispatcher();
            GameTime = new Cosmos.GameTime();

            Rendering.VertexTypes.VertexTypeProvider.Initialize(GraphicsDevice);

            initialized = true;
        }

        public static void Run()
        {
            Debug.Assert(initialized);
            GameTime.Refresh();
            RenderLoop.Run(GameForm, new RenderLoop.RenderCallback(Tick));
        }

        static void Tick()
        {
            GameTime.Refresh();
            Profiler.UpdateFps(GameTime);
            Profiler.UpdateProbeProfiles(GameTime);
            UpdateLoop.Update(GameTime);
        }

        public static void FinalizeSystem()
        {
            Rendering.VertexTypes.VertexTypeProvider.Dispose();

            if (DebugOutput != null) DebugOutput.Dispose(); DebugOutput = null;
            Log = null;
            if (GraphicsDevice != null) GraphicsDevice.Dispose(); GraphicsDevice = null;
            if (GameForm != null) GameForm.Dispose(); GameForm = null;
        }

        #region privates

        static bool initialized = false;

        #endregion
    }
}

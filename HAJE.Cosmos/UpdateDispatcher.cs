﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAJE.Cosmos
{
    public interface IUpdatable
    {
        void Update(GameTime gameTime);
    }

    public class UpdateDispatcher
    {
        public UpdateDispatcher()
        {
        }

        public void Register(IUpdatable loop)
        {
            tobeRemoved.Remove(loop);
            tobeAdded.Add(loop);
        }

        public IUpdatable Register(Action<GameTime> func)
        {
            var ret = new UpdateHandler(func);
            tobeAdded.Add(ret);
            return ret;
        }

        public IUpdatable Register(Action func)
        {
            var ret = new VoidUpdateHandler(func);
            return ret;
        }

        public void Hide(IUpdatable loop)
        {
            tobeAdded.Remove(loop);
            tobeRemoved.Add(loop);
        }

        public void Update(GameTime gameTime)
        {
            foreach (var l in tobeAdded)
                updateList.Add(l);
            foreach (var l in tobeRemoved)
                updateList.Remove(l);
            tobeAdded.Clear();
            tobeRemoved.Clear();
            foreach (var l in updateList)
                l.Update(gameTime);
        }

        class UpdateHandler : IUpdatable
        {
            public Action<GameTime> func;

            public UpdateHandler(Action<GameTime> func)
            {
                this.func = func;
            }

            public void Update(GameTime time)
            {
                func(time);
            }
        }

        class VoidUpdateHandler : IUpdatable
        {
            public Action func;

            public VoidUpdateHandler(Action func)
            {
                this.func = func;
            }

            public void Update(GameTime time)
            {
                func();
            }
        }

        List<IUpdatable> updateList = new List<IUpdatable>();
        List<IUpdatable> tobeAdded = new List<IUpdatable>();
        List<IUpdatable> tobeRemoved = new List<IUpdatable>();
    }
}

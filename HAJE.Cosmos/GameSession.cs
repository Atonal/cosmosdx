﻿using HAJE.Cosmos.Combat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAJE.Cosmos
{
    public class GameSession
    {
        public static GameSession RunningSession
        {
            get;
            private set;
        }

        public int MaxPlayer
        {
            get;
            private set;
        }

        public Network.Local.Server Server
        {
            get;
            private set;
        }

        public Network.Local.Client Client
        {
            get;
            private set;
        }

        public bool IsOwner
        {
            get
            {
                return Server != null;
            }
        }

        public CombatSystem CombatSystem
        {
            get;
            private set;
        }

        public CombatLoop CombatLoop
        {
            get;
            private set;
        }

        public static void CreateSession(Action sessionCreated)
        {
            if (RunningSession != null)
                throw new InvalidOperationException("이미 실행 중인 게임 세션이 있습니다.");

            var session = new GameSession();
            session.MaxPlayer = 1;
            session.Server = new Network.Local.Server();
            session.Client = new Network.Local.Client();
            session.Server.Start();
            session.Client.Connect("localhost");
            GameSystem.UpdateLoop.Register(session.Server);
            GameSystem.UpdateLoop.Register(session.Client);

            session.Server.OnConnectionChanged += sessionCreated;
            RunningSession = session;
        }

        public void BeginCombat()
        {
            CombatLoop = new Combat.CombatLoop();
            CombatLoop.Initialize();

            CombatSystem = new CombatSystem(this, CombatLoop);
            CombatSystem.Begin();
            CombatLoop.CombatSystem = CombatSystem;
        }

        public void End()
        {
            if (CombatLoop != null)
            {
                CombatLoop.Dispose();
                CombatLoop = null;
            }
            if (CombatSystem != null)
            {
                CombatSystem.End();
                CombatSystem = null;
            }
            if (Server != null)
            {
                GameSystem.UpdateLoop.Hide(Server);
                Server.End();
                Server = null;
            }
            if (Client != null)
            {
                GameSystem.UpdateLoop.Hide(Client);
                Client.Disconnect();
                Client = null;
            }
        }

        // 직접 생성 금지.
        // Create(새 세션 생성) 혹은 Join(기존 세션 접속)등만 가능하다.
        // 기존 세션에서 호스트가 나간 경우 이어받는 처리 등이 필요 할 수도 있다.
        private GameSession()
        {
        }
    }
}

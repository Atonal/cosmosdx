﻿using HAJE.Cosmos.Network.Local;
using System;
using System.Collections.Generic;

namespace HAJE.Cosmos
{
    static class Entry
    {
        static void Main()
        {
            GameSystem.Initialize();
            GameSession.CreateSession(() =>
            {
                GameSession.RunningSession.BeginCombat();
            });

            GameSystem.Run();
            GameSession.RunningSession.End();
            GameSystem.FinalizeSystem();
        }
    }
}

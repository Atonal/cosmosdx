﻿using SharpDX;
using SharpDX.Direct3D9;

namespace HAJE.Cosmos
{
    public static class TextureExtension
    {
        public static Vector2 GetSize(this Texture texture)
        {
            SurfaceDescription desc = texture.GetLevelDescription(0);
            return new Vector2(desc.Width, desc.Height);
        }
    }
}

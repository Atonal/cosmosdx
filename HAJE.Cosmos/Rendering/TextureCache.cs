﻿using SharpDX.Direct3D9;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace HAJE.Cosmos.Rendering
{
    public class TextureCache : IDisposable
    {
        public TextureCache(Device graphicsDevice)
        {
            this.graphicsDevice = graphicsDevice;
        }

        public Texture GetTexture(string path)
        {
            Texture tex;
            if (cache.TryGetValue(path, out tex))
                return tex;
            
            if (!File.Exists(path))
                throw new FileNotFoundException();

            int width, height;
            using (var img = Image.FromFile(path))
            {
                width = img.Width;
                height = img.Height;
            }
            tex = Texture.FromFile(graphicsDevice, path, width, height, 1, Usage.None, Format.A8R8G8B8, Pool.Managed, Filter.Point, Filter.Point, 0);
            cache.Add(path, tex);
            return tex;
        }

        public void Clear()
        {
            foreach (Texture t in cache.Values)
            {
                t.Dispose();
            }
            cache.Clear();
        }

        public void Dispose()
        {
            Clear();
        }

        Device graphicsDevice;
        Dictionary<string, Texture> cache = new Dictionary<string, Texture>();
    }
}

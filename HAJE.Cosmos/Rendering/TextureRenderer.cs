﻿using HAJE.Cosmos.Rendering.VertexTypes;
using SharpDX;
using SharpDX.Direct3D9;
using System;
using System.Diagnostics;

namespace HAJE.Cosmos.Rendering
{
    public class TextureRenderer : IDisposable
    {
        public TextureRenderer(Device graphicsDevice)
        {
            device = graphicsDevice;
            vertexBuffer = new VertexBuffer(device,
                vertices.Length * TransformedPositionColorTextureVertexType.Size,
                Usage.WriteOnly,
                VertexFormat.None,
                Pool.Managed
            ); 
        }

        public void DrawTexture(TextureFrame texture, Vector2 position)
        {
            Debug.Assert(texture.Texture != null);

            DrawTexture(texture, null, position, Vector2.Zero, new Vector2(1, 1), (Radian)0);
        }

        public void DrawTexture(TextureFrame texture, Color? tint, Vector2 position, Vector2 anchor, Vector2 scale, Radian rotation)
        {
            Debug.Assert(texture.Texture != null);

            RectangleF uvRect = texture.UVMap;

            tint = tint ?? Color.White;
            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i].Color = tint.Value;
                vertices[i].Position = new Vector4(position, 0, 1);
            }

            float scaledWidth = texture.Frame.Width * scale.X;
            float scaledHeight = texture.Frame.Height * scale.Y;
            Vector2 tl = new Vector2(-anchor.X * scaledWidth, -anchor.Y * scaledHeight);
            Vector2 tr = new Vector2((1 - anchor.X) * scaledWidth, -anchor.Y * scaledHeight);
            Vector2 bl = new Vector2(-anchor.X * scaledWidth, (1 - anchor.Y) * scaledHeight);
            Vector2 br = new Vector2((1 - anchor.X) * scaledWidth, (1 - anchor.Y) * scaledHeight);
            tl = rotation.Rotate(tl);
            tr = rotation.Rotate(tr);
            bl = rotation.Rotate(bl);
            br = rotation.Rotate(br);

            vertices[0].TextureUV = uvRect.TopLeft;
            vertices[0].Position.X += tl.X;
            vertices[0].Position.Y += tl.Y;

            vertices[1].TextureUV = uvRect.TopRight;
            vertices[1].Position.X += tr.X;
            vertices[1].Position.Y += tr.Y;

            vertices[2].TextureUV = uvRect.BottomLeft;
            vertices[2].Position.X += bl.X;
            vertices[2].Position.Y += bl.Y;

            vertices[3].TextureUV = uvRect.TopRight;
            vertices[3].Position.X += tr.X;
            vertices[3].Position.Y += tr.Y;

            vertices[4].TextureUV = uvRect.BottomRight;
            vertices[4].Position.X += br.X;
            vertices[4].Position.Y += br.Y;

            vertices[5].TextureUV = uvRect.BottomLeft;
            vertices[5].Position.X += bl.X;
            vertices[5].Position.Y += bl.Y;

            using (DataStream buffer = vertexBuffer.Lock(0, 0, LockFlags.None))
            {
                for (int i = 0; i < vertices.Length; i++)
                    vertices[i].WriteTo(buffer);
                vertexBuffer.Unlock();
            }

            device.SetTexture(0, texture.Texture);
            device.SetRenderState(RenderState.Lighting, false);
            device.SetRenderState(RenderState.ZEnable, false);
            device.SetSamplerState(0, SamplerState.MagFilter, TextureFilter.Linear);
            device.SetSamplerState(0, SamplerState.MinFilter, TextureFilter.Linear);
            device.SetSamplerState(0, SamplerState.MipFilter, TextureFilter.Linear);
            device.SetSamplerState(0, SamplerState.AddressU, TextureAddress.Clamp);
            device.SetSamplerState(0, SamplerState.AddressV, TextureAddress.Clamp);
            device.SetRenderState(RenderState.AlphaBlendEnable, true);
            device.SetRenderState(RenderState.SourceBlend, Blend.SourceAlpha);
            device.SetRenderState(RenderState.DestinationBlend, Blend.InverseSourceAlpha);
            device.SetRenderState(RenderState.SeparateAlphaBlendEnable, true);
            device.SetRenderState(RenderState.SourceBlendAlpha, Blend.SourceAlpha);
            device.SetRenderState(RenderState.DestinationBlendAlpha, Blend.InverseSourceAlpha);
            device.SetRenderState(RenderState.BlendOperationAlpha, BlendOperation.Add);
            device.SetStreamSource(0, vertexBuffer, 0, TransformedPositionColorTextureVertexType.Size);
            device.VertexDeclaration = TransformedPositionColorTextureVertexType.VertexDeclaration;
            device.DrawPrimitives(PrimitiveType.TriangleList, 0, triangleCount);
        }

        public void Dispose()
        {
            if (vertexBuffer != null) vertexBuffer.Dispose(); vertexBuffer = null;
        }

        Device device;
        const int triangleCount = 2;
        TransformedPositionColorTextureVertexType[] vertices = new TransformedPositionColorTextureVertexType[triangleCount * 3];
        VertexBuffer vertexBuffer;
    }
}

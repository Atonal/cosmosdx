﻿using SharpDX;
using SharpDX.Direct3D9;
using System;
using System.Collections.Generic;

namespace HAJE.Cosmos.Rendering
{
    public class TextureAtlas : IDisposable
    {
        public TextureAtlas(Device device, Size2 size)
        {
            graphicsDevice = device;
            width = size.Width;
            height = size.Height;
            texture = new Texture(graphicsDevice,
                size.Width, size.Height,
                1, SharpDX.Direct3D9.Usage.RenderTarget,
                SharpDX.Direct3D9.Format.A8R8G8B8, SharpDX.Direct3D9.Pool.Default
            );
            surface = texture.GetSurfaceLevel(0);
            emptyList = new List<Point>();
            emptyList.Add(new Point(0, 0));
            textureRenderer = new TextureRenderer(graphicsDevice);
        }

        public TextureFrame RegisterTexture(TextureFrame texture, Size2 targetSize, AtlasBuildAction buildAction)
        {
            var allocated = Allocate(targetSize);
            var ret = new TextureFrame(this.texture, allocated);
            RegisteredAtlasDescriptor r = new RegisteredAtlasDescriptor();
            r.SourceTexture = texture;
            r.BuildAction = buildAction;
            r.AreaInAtlas = allocated;
            r.AllocatedUV = ret.UVMap;
            registered.Add(r);
            return ret;
        }

        public TextureFrame RegisterTexture(TextureFrame texture, Size2 targetSize)
        {
            return RegisterTexture(texture, targetSize, DefaultAtlasBuildAction);
        }

        public TextureFrame RegisterTexture(TextureFrame texture)
        {
            return RegisterTexture(texture, texture.Frame.Size);
        }

        public void BuildAtlas()
        {
            var oldTarget = graphicsDevice.GetRenderTarget(0);
            graphicsDevice.SetRenderTarget(0, surface);
            graphicsDevice.Clear(ClearFlags.Target, new Color(0, 0, 0, 0), 0, 0);
            graphicsDevice.BeginScene();
            for (int i = 0; i < registered.Count; i++)
            {
                registered[i].BuildAction(textureRenderer,
                    ref registered[i].SourceTexture,
                    ref registered[i].AreaInAtlas,
                    ref registered[i].AllocatedUV
                );
            }

            graphicsDevice.EndScene();
            graphicsDevice.SetRenderTarget(0, oldTarget);
        }

        public void Dispose()
        {
            if (surface != null) surface.Dispose(); surface = null;
            if (texture != null) texture.Dispose(); surface = null;
            if (textureRenderer != null) textureRenderer.Dispose(); textureRenderer = null;
        }

        public static void DefaultAtlasBuildAction(TextureRenderer renderer, ref TextureFrame source, ref Rectangle area, ref RectangleF atlasUV)
        {
            Vector2 pos = new Vector2(area.Left, area.Top);
            Vector2 scale = new Vector2((float)area.Width / source.Frame.Width, (float)area.Height / source.Frame.Height);
            renderer.DrawTexture(source, null, pos, Vector2.Zero, scale, (Radian)0.0f);
        }

        public Texture Texutre
        {
            get
            {
                return texture;
            }
        }

        public Size2 Size
        {
            get
            {
                return new Size2(width, height);
            }
        }

        #region area allocation

        private Rectangle Allocate(Size2 size)
        {
            int optimalIndex = emptyList.Count;
            int optimalHeight = height;

            // 추가 가능한 영역들 중 높이가 가장 작은 영역을 검색
            for (int i = 0; i < emptyList.Count - 1; i++)
            {
                int emptyHeight = emptyList[i + 1].Y - emptyList[i].Y;
                if (emptyHeight < optimalHeight 
                    && emptyHeight >= size.Height 
                    && IsAllocatable(emptyList[i], size))
                {
                    optimalIndex = i;
                    optimalHeight = emptyHeight;
                }
            }

            // 최적 영역의 높이와 입력 높이가 너무 많이 차이나는 경우 예외 처리
            if (optimalHeight * 0.75 > size.Height 
                && IsAllocatable(emptyList[emptyList.Count - 1], size))
            {
                optimalIndex = emptyList.Count;
            }

            // 새로운 영역으로 추가해야 하는 상황이지만,
            // 마지막 영역의 높이와 입력 높이가 별로 차이나지 않는 경우 예외처리
            if (emptyList.Count == optimalIndex && emptyList.Count > 1)
            {
                int last = emptyList.Count - 1;
                int lastHeight = emptyList[last].Y - emptyList[last - 1].Y;
                if (size.Height > lastHeight
                    && lastHeight * 1.10 > size.Height
                    && IsAllocatable(emptyList[last - 1], size))
                {
                    Rectangle rect = new Rectangle()
                    {
                        Location = emptyList[last - 1],
                        Size = size
                    };
                    emptyList[last - 1] = new Point(rect.Right + padding, rect.Top);
                    emptyList[last] = new Point(0, rect.Bottom + padding);
                    return rect;
                }
            }

            if (emptyList.Count == optimalIndex)
            {
                // 아래쪽에 새로운 영역으로 추가
                Rectangle rect = new Rectangle()
                {
                    Location = emptyList[emptyList.Count - 1],
                    Size = size
                };
                if (IsAllocatable(rect))
                {
                    emptyList.RemoveAt(emptyList.Count - 1);
                    emptyList.Add(new Point(rect.Right + padding, rect.Top));
                    emptyList.Add(new Point(0, rect.Bottom + padding));
                    return rect;
                }
            }
            else
            {
                // 기존 영역의 오른쪽에 추가
                Rectangle rect = new Rectangle()
                {
                    Location = emptyList[optimalIndex],
                    Size = size
                };
                if (IsAllocatable(rect))
                {
                    emptyList[optimalIndex] = new Point(rect.Right + padding, rect.Top);
                    return rect;
                }
            }

            throw new InvalidOperationException("TextureAtlas에 더 이상 공간이 없습니다.");
        }

        private bool IsAllocatable(Point position, Size2 size)
        {
            Rectangle rect = new Rectangle()
            {
                Location = position,
                Size = size
            };
            return IsAllocatable(rect);
        }

        private bool IsAllocatable(Rectangle rect)
        {
            if (rect.Right > width || rect.Bottom > height)
                return false;

            foreach (RegisteredAtlasDescriptor d in registered)
            {
                if (rect.Intersects(d.AreaInAtlas))
                {
                    return false;
                }
            }
            return true;
        }

        class RegisteredAtlasDescriptor
        {
            public TextureFrame SourceTexture;
            public Rectangle AreaInAtlas;
            public AtlasBuildAction BuildAction;
            public RectangleF AllocatedUV;
        };

        List<RegisteredAtlasDescriptor> registered = new List<RegisteredAtlasDescriptor>();
        List<Point> emptyList;

        #endregion

        const int padding = 2;
        int width, height;
        Texture texture;
        Surface surface;
        Device graphicsDevice;
        TextureRenderer textureRenderer;
    }

    public delegate void AtlasBuildAction(TextureRenderer renderer, ref TextureFrame source, ref Rectangle area, ref RectangleF atlasUV);
}

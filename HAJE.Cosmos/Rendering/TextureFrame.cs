﻿using SharpDX;
using SharpDX.Direct3D9;
using System;

namespace HAJE.Cosmos.Rendering
{
    public struct TextureFrame
    {
        public TextureFrame(Texture texture)
        {
            this.Texture = texture;
            var size = texture.GetSize();
            var magic = Vector2.One / size / 2.0f; ;
            this.UVMap = new RectangleF(magic.X, magic.Y, 1, 1);
        }

        public TextureFrame(Texture texture, Rectangle frame)
        {
            this.Texture = texture;
            var texSize = texture.GetSize();
            var magic = Vector2.One / texSize / 2.0f; ;
            this.UVMap = new RectangleF(
                (float)frame.X / texSize.X + magic.X,
                (float)frame.Y / texSize.Y + magic.Y,
                (float)frame.Width / texSize.X,
                (float)frame.Height / texSize.Y
            );
        }

        public TextureFrame(Texture texture, RectangleF uv)
        {
            this.Texture = texture;
            this.UVMap = uv;
        }

        public readonly Texture Texture;
        public readonly RectangleF UVMap;

        public Rectangle Frame
        {
            get
            {
                var texSize = Texture.GetSize();
                return new Rectangle(
                    (int)Math.Floor(UVMap.X * texSize.X),
                    (int)Math.Floor(UVMap.Y * texSize.Y),
                    (int)Math.Ceiling(UVMap.Width * texSize.X),
                    (int)Math.Ceiling(UVMap.Height * texSize.Y)
                );
            }
        }

        public static implicit operator TextureFrame(Texture texture)
        {
            return new TextureFrame(texture);
        }
    }
}

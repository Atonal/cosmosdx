﻿using SharpDX;
using SharpDX.Direct3D9;
using System;
using System.Runtime.InteropServices;

namespace HAJE.Cosmos.Rendering.VertexTypes
{
    public struct PositionColorVertexType
    {
        public Vector3 Position;
        public Color Color;

        public static readonly int Size = Marshal.SizeOf(typeof(PositionColorVertexType));

        public static VertexDeclaration VertexDeclaration
        {
            private set;
            get;
        }

        public void WriteTo(DataStream dataStream)
        {
            dataStream.Write(Position);
            dataStream.Write((ColorBGRA)Color);
        }

        internal static void SetupDeclaration(Device device)
        {
            VertexElement[] elements = new[]{
                new VertexElement(0, 0, DeclarationType.Float3, DeclarationMethod.Default, DeclarationUsage.Position, 0),
                new VertexElement(0, 12, DeclarationType.Color, DeclarationMethod.Default, DeclarationUsage.Color, 0),
                VertexElement.VertexDeclarationEnd
            };
            VertexDeclaration = new VertexDeclaration(
                device,
                elements
            );
        }

        internal static void DisposeDeclaration()
        {
            if (VertexDeclaration != null)
            {
                VertexDeclaration.Dispose();
            }
        }
    }
}

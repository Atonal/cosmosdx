﻿using SharpDX;
using SharpDX.Direct3D9;
using System;
using System.Runtime.InteropServices;

namespace HAJE.Cosmos.Rendering.VertexTypes
{
    public struct PositionColorTextureVertexType
    {
        public Vector4 Position;
        public Color Color;
        public Vector2 TextureUV;

        public static readonly int Size = Marshal.SizeOf(typeof(PositionColorTextureVertexType));
        
        public static VertexDeclaration VertexDeclaration
        {
            private set;
            get;
        }

        public void WriteTo(DataStream dataStream)
        {
            dataStream.Write(Position);
            dataStream.Write((ColorBGRA)Color);
            dataStream.Write(TextureUV);
        }

        static internal void SetupDeclaration(Device device)
        {
            VertexElement[] elements = new[]{
                new VertexElement(0, 0, DeclarationType.Float4, DeclarationMethod.Default, DeclarationUsage.Position, 0),
                new VertexElement(0, 16, DeclarationType.Color, DeclarationMethod.Default, DeclarationUsage.Color, 0),
                new VertexElement(0, 20, DeclarationType.Float2, DeclarationMethod.Default, DeclarationUsage.TextureCoordinate, 0),
                VertexElement.VertexDeclarationEnd
            };
            VertexDeclaration = new VertexDeclaration(
                device,
                elements
            );
        }

        static internal void DisposeDeclaration()
        {
            VertexDeclaration.Dispose();
        }
    }
}

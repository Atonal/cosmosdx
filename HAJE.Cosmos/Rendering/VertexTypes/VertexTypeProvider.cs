﻿using SharpDX.Direct3D9;

namespace HAJE.Cosmos.Rendering.VertexTypes
{
    public class VertexTypeProvider
    {
        public static void Initialize(Device device)
        {
            PositionColorVertexType.SetupDeclaration(device);
            PositionColorTextureVertexType.SetupDeclaration(device);
            TransformedPositionColorTextureVertexType.SetupDeclaration(device);
        }

        public static void Dispose()
        {
            PositionColorVertexType.DisposeDeclaration();
            PositionColorTextureVertexType.DisposeDeclaration();
            TransformedPositionColorTextureVertexType.DisposeDeclaration();
        }
    }
}

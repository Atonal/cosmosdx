﻿using SharpDX;
using SharpDX.Direct3D9;
using System.Runtime.InteropServices;

namespace HAJE.Cosmos.Rendering.VertexTypes
{
    /// <summary>
    /// 화면상의 위치, 색상, 텍스쳐 위치를 담고 있는 정점 타입
    /// </summary>
    public struct TransformedPositionColorTextureVertexType
    {
        /// <summary>
        /// 정점의 위치
        /// </summary>
        public Vector4 Position;

        /// <summary>
        /// 정점의 색
        /// </summary>
        public Color Color;

        /// <summary>
        /// 정점의 텍스쳐 안의 좌표
        /// </summary>
        public Vector2 TextureUV;

        /// <summary>
        /// 정점 하나의 크기 (단위: 바이트)
        /// </summary>
        public static readonly int Size = Marshal.SizeOf(typeof(TransformedPositionColorTextureVertexType));

        /// <summary>
        /// 정점 정보 내용
        /// </summary>
        public static VertexDeclaration VertexDeclaration
        {
            private set;
            get;
        }

        /// <summary>
        /// 대상 정점 정보 스트림에 이 정점의 내용을 기록합니다.
        /// </summary>
        public void WriteTo(DataStream dataStream)
        {
            dataStream.Write(Position);
            dataStream.Write((ColorBGRA)Color);
            dataStream.Write(TextureUV);
        }

        static internal void SetupDeclaration(Device device)
        {
            VertexElement[] elements = new[]{
                new VertexElement(0, 0, DeclarationType.Float4, DeclarationMethod.Default, DeclarationUsage.PositionTransformed, 0),
                new VertexElement(0, 16, DeclarationType.Color, DeclarationMethod.Default, DeclarationUsage.Color, 0),
                new VertexElement(0, 20, DeclarationType.Float2, DeclarationMethod.Default, DeclarationUsage.TextureCoordinate, 0),
                VertexElement.VertexDeclarationEnd
            };
            VertexDeclaration = new VertexDeclaration(device, elements);
        }

        static internal void DisposeDeclaration()
        {
            VertexDeclaration.Dispose();
        }
    }
}

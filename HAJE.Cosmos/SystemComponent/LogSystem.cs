﻿using SharpDX;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.Cosmos.SystemComponent
{
    public class LogSystem
    {
        public void WriteLine(string text)
        {
            const int maxLine = 150;

            if (Debugger.IsAttached)
                Debug.WriteLine(text);

            Log.Enqueue(text);
            while (Log.Count > maxLine)
                Log.Dequeue();
        }

        public Queue<string> Log = new Queue<string>();
    }
}

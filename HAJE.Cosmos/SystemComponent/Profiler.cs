﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace HAJE.Cosmos.SystemComponent
{
    public class Profiler
    {
        public Profiler()
        {
        }

        public FpsData Fps
        {
            get
            {
                return presentingFps;
            }
        }

        public void UpdateFps(GameTime gameTime)
        {
            RecordFps(gameTime);
            if (IsFpsSampleComplete())
            {
                PresentFpsSample();
            }
        }

        #region fps related

        const int minimumSamples = 5;
        const int maximumSamples = 30;
        readonly Second minimumTimeSlice = (Second)1.0f;
        int fpsSampleCount;
        FpsData recordingFps;
        FpsData presentingFps;

        private void RecordFps(GameTime gameTime)
        {
            fpsSampleCount++;
            recordingFps.SampleInterval += gameTime.DeltaTime;

            float dt = gameTime.DeltaTime;
            presentingFps.Current = recordingFps.Current = 1 / dt;
            recordingFps.Maximum = Math.Max(recordingFps.Maximum, recordingFps.Current);
            recordingFps.Minimum = Math.Min(recordingFps.Minimum, recordingFps.Current);
            recordingFps.Average = fpsSampleCount / recordingFps.SampleInterval;
        }

        private bool IsFpsSampleComplete()
        {
            if (fpsSampleCount < minimumSamples) return false;
            if (fpsSampleCount >= maximumSamples) return true;
            return recordingFps.SampleInterval >= minimumTimeSlice;
        }

        private void PresentFpsSample()
        {
            presentingFps = recordingFps;
            recordingFps.Average = 0;
            recordingFps.Maximum = float.MinValue;
            recordingFps.Minimum = float.MaxValue;
            recordingFps.SampleInterval = (Second)0;
            fpsSampleCount = 0;
        }

        #endregion

        public IDisposable GetProbe(string routineName)
        {
            BeginProbe(routineName);
            return probes[routineName];
        }

        public void BeginProbe(string routineName)
        {
            if (!probes.ContainsKey(routineName))
            {
                var newProbe = new ProbeData();
                newProbe.RecentTime.SetBreakPointSafeValue(samplingLength, samplingLength);
                newProbe.RoutineName = routineName;
                probes.Add(routineName, newProbe);
            }
            var p = probes[routineName];
            if (p.StackCount == 0)
                p.RecentTime.Refresh();
            p.StackCount++;
        }

        public void EndProbe(string routineName)
        {
            Debug.Assert(probes.ContainsKey(routineName));
            var p = probes[routineName];
            Debug.Assert(p.StackCount > 0);
            p.StackCount--;
            if (p.StackCount == 0)
            {
                p.RecentTime.Refresh();
                p.UpTime += p.RecentTime.DeltaTime;
            }
        }

        public void UpdateProbeProfiles(GameTime gameTime)
        {
            sampleInteval += gameTime.DeltaTime;
            if (sampleInteval >= samplingLength)
            {
                probeResults.Clear();

                foreach (var p in probes.Values)
                {
                    if (p.StackCount > 0)
                    {
                        p.RecentTime.Refresh();
                        p.UpTime += p.RecentTime.DeltaTime;
                    }

                    Second uptimePerFrame = (Second)(p.UpTime / sampleInteval / presentingFps.Average);
                    probeResults.Add(p.RoutineName, uptimePerFrame);

                    p.UpTime = (Second)0.0f;
                }
                sampleInteval = (Second)0.0f;
            }
        }

        public Dictionary<string, Second> ProbeProfiles
        {
            get
            {
                return probeResults;
            }
        }

        #region probe related

        Dictionary<string, Second> probeResults = new Dictionary<string, Second>();
        readonly Second samplingLength = (Second)1.0f;
        Second sampleInteval = (Second)0.0f;

        class ProbeData : IDisposable
        {
            public void Dispose()
            {
                GameSystem.Profiler.EndProbe(RoutineName);
            }

            public string RoutineName;
            public int StackCount = 0;
            public GameTime RecentTime = new GameTime();
            public Second UpTime = (Second)0.0f;
        }
        Dictionary<string, ProbeData> probes = new Dictionary<string, ProbeData>();

        #endregion
    }

    public struct FpsData
    {
        public float Current;
        public float Average;
        public float Minimum;
        public float Maximum;
        public Second SampleInterval;
    }
}

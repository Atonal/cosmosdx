﻿using Lidgren.Network;
using System.Collections.Generic;

namespace HAJE.Cosmos.Network
{
    public class PacketDispatcher
    {
        public PacketDispatcher()
        {
            registered = new Dictionary<int, PacketBase>();
        }

        public void Register(PacketBase packet)
        {
            registered.Add(packet.Code, packet);
        }

        public void Hide(PacketBase packet)
        {
            registered.Remove(packet.Code);
        }

        public void HandleMessage(NetIncomingMessage msg)
        {
            PacketBase packet;
            if (registered.TryGetValue(PacketBase.PeekCode(msg), out packet))
            {
                packet.ReadMessage(msg);
            }
        }

        #region privates

        Dictionary<int, PacketBase> registered;

        #endregion
    }
}

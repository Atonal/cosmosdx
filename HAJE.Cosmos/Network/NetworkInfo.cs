﻿using System.Windows.Forms;

namespace HAJE.Cosmos.Network
{
    public static class NetworkInfo
    {
        public static readonly string LocalAppName = Application.ProductName + "LocalServer";
        public static readonly int Port = 31415;
    }
}

﻿using HAJE.Cosmos.Input;

namespace HAJE.Cosmos.Network.Local
{
    public struct InputCommand
    {
        public enum CommandType
        {
            KeyboardUp,
            KeyboardDown,
            KeyboardDouble,
            MouseUp,
            MouseDown,
            MouseDouble
        }

        public CommandType Type;
        public Keys KeyboardKey;
        public MouseButtons MouseButton;

        public bool IsMouseCommand
        {
            get
            {
                switch (Type)
                {
                    case CommandType.MouseDouble:
                    case CommandType.MouseDown:
                    case CommandType.MouseUp:
                        return true;
                }
                return false;
            }
        }
        public byte CommandParameter
        {
            get
            {
                if (IsMouseCommand)
                    return (byte)MouseButton;
                else
                    return (byte)KeyboardKey;
            }
            set
            {
                if (IsMouseCommand)
                    MouseButton = (MouseButtons)value;
                else
                    KeyboardKey = (Keys)value;
            }
        }

        public override string ToString()
        {
            if (IsMouseCommand)
            {
                return Type + ": " + MouseButton;
            }
            else
            {
                return Type + ": " + KeyboardKey;
            }
        }
    }
}

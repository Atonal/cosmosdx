﻿using Lidgren.Network;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

namespace HAJE.Cosmos.Network.Local
{
    public class Server : IUpdatable
    {
        public Server()
        {
            NetPeerConfiguration config = new NetPeerConfiguration(NetworkInfo.LocalAppName);
            config.Port = NetworkInfo.Port;
            config.MaximumConnections = 1;
            config.PingInterval = 1.5f;
            if (GameSystem.BuildInfo.DebugBuild || Debugger.IsAttached)
                config.ConnectionTimeout = 600.0f;
            else
                config.ConnectionTimeout = 5.0f;
            config.AutoFlushSendQueue = true;
            config.EnableMessageType(NetIncomingMessageType.DebugMessage);
            config.EnableMessageType(NetIncomingMessageType.VerboseDebugMessage);
            server = new NetServer(config);
        }

        public readonly PacketDispatcher PacketDispatcher = new PacketDispatcher();
        public readonly UpdateDispatcher UpdateDispatcher = new UpdateDispatcher();
        public event Action OnConnectionChanged = delegate { };
        public event Action<string> OnUnhandledMessage = delegate { };

        public void Start()
        {
            server.Start();
        }

        public void End()
        {
            server.Shutdown("Server Closed");
        }

        public void Update(GameTime gameTime)
        {
            NetIncomingMessage msg;
            while ((msg = server.ReadMessage()) != null)
            {
                switch (msg.MessageType)
                {
                    case NetIncomingMessageType.DebugMessage:
                    case NetIncomingMessageType.ErrorMessage:
                    case NetIncomingMessageType.WarningMessage:
                    case NetIncomingMessageType.VerboseDebugMessage:
                        OnUnhandledMessage(msg.ReadString());
                        break;
                    case NetIncomingMessageType.StatusChanged:
                        NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte();
                        connections = server.Connections;
                        if (status == NetConnectionStatus.Connected || status == NetConnectionStatus.Disconnected)
                            OnConnectionChanged();
                        string reason = msg.ReadString();
                        break;
                    case NetIncomingMessageType.Data:
                        recentMessage = msg;
                        PacketDispatcher.HandleMessage(msg);
                        recentMessage = null;
                        break;
                }
                server.Recycle(msg);
            }

            UpdateDispatcher.Update(gameTime);
        }

        public void SendPacket(PacketBase packet)
        {
            NetOutgoingMessage msg = server.CreateMessage();
            packet.WriteMessage(msg);
            server.SendMessage(msg, connections, packet.DeliveryMethod, packet.SequenceChannel);
        }

        public int ResolveConnectionIndex(NetIncomingMessage msg)
        {
            // 중간에 갑자기 접속이 끊겨 ConnectionList의 인덱스가
            // 변동되는 경우 책임지지 않는다.
            for (int i = 0; i < connections.Count; i++)
            {
                if (connections[i] == msg.SenderConnection)
                    return i;
            }
            return -1;
        }

        public int GetRecentlyReceivedMessageConnectionIndex()
        {
            // 멀티스레드로 동작하는 순간 올바르게 동작한다는 보장을 얻을 수 없다.
            return ResolveConnectionIndex(recentMessage);
        }

        #region privates

        NetServer server;
        List<NetConnection> connections;
        NetIncomingMessage recentMessage;

        #endregion
    }
}

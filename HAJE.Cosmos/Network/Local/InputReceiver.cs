﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAJE.Cosmos.Network.Local
{
    public class InputReceiver
    {
        public InputReceiver(Client client, int playerCount)
        {
            this.client = client;
            this.playerCount = playerCount;
            this.inputPacket = new Packet.ServerToClient.FrameInput(playerCount);
            this.inputPacket.OnReceived += inputPacket_OnReceived;
        }

        public void Begin()
        {
            client.PacketDispatcher.Register(inputPacket);
            frameIndex = 0;
        }

        public void End()
        {
            client.PacketDispatcher.Hide(inputPacket);
        }

        public FrameInput FetchInput()
        {
            FrameInput frame;
            if (frameBuffer.TryGetValue(frameIndex, out frame))
            {
                // Queue에 넣어서 재활용 해야 하기 때문에,
                // Fetch와 NextInput을 분리해야 함.
                return frame;
            }
            return null;
        }

        public void NextInput()
        {
            FrameInput frame = frameBuffer[frameIndex];
            frameBuffer.Remove(frameIndex);
            frameIndex++;
            garbagePool.Enqueue(frame);
        }

        public int BufferCount
        {
            get
            {
                return frameBuffer.Count;
            }
        }

        #region privates

        void inputPacket_OnReceived()
        {
            FrameInput frame;
            if (garbagePool.Count() > 0)
                frame = garbagePool.Dequeue();
            else
                frame = new FrameInput(playerCount);
            frame.CopyFrom(inputPacket.Frame);

            frameBuffer.Add(frame.FrameIndex, frame);
        }

        Client client;
        Packet.ServerToClient.FrameInput inputPacket;
        SortedList<int, FrameInput> frameBuffer = new SortedList<int, FrameInput>();
        Queue<FrameInput> garbagePool = new Queue<FrameInput>();
        int playerCount;
        int frameIndex;

        #endregion
    }
}

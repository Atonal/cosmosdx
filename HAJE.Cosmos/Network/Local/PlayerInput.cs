﻿using System.Collections.Generic;

namespace HAJE.Cosmos.Network.Local
{
    public class PlayerInput
    {
        public void CopyFrom(PlayerInput source)
        {
            MouseX = source.MouseX;
            MouseY = source.MouseY;
            CommandList.Clear();
            for(int i=0; i<source.CommandList.Count; i++)
                CommandList.Add(source.CommandList[i]);
        }

        public float MouseX;
        public float MouseY;
        public List<InputCommand> CommandList = new List<InputCommand>();
    }
}

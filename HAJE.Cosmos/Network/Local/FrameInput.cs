﻿using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.Cosmos.Network.Local
{
    public class FrameInput
    {
        public static readonly Second DefaultFrameInterval = (Second)(1.0f / 15);

        public FrameInput(int playerCount)
        {
            Inputs = new PlayerInput[playerCount];
            for (int i = 0; i < Inputs.Length; i++)
            {
                Inputs[i] = new PlayerInput();
            }
        }

        public void CopyFrom(FrameInput source)
        {
            Debug.Assert(Inputs.Length == source.Inputs.Length);
            FrameIndex = source.FrameIndex;
            FrameLength = source.FrameLength;
            for (int i = 0; i < Inputs.Length; i++)
                Inputs[i].CopyFrom(source.Inputs[i]);
        }

        public void Initialize(int frameIndex)
        {
            FrameIndex = frameIndex;
            FrameLength = DefaultFrameInterval;
            for (int i = 0; i < Inputs.Length; i++)
            {
                Inputs[i].CommandList.Clear();
            }
        }

        public int FrameIndex;
        public Second FrameLength;
        public PlayerInput[] Inputs;
    }
}

﻿using HAJE.Cosmos.Input;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.Cosmos.Network.Local
{
    public class InputSender : IUpdatable 
    {
        #region constructor

        public InputSender(Client client)
        {
            this.packet = new Packet.ClientToServer.SendCommand();
            this.client = client;
            this.recentUpList = new List<DoubleKeyTime>();
        }

        #endregion

        public void Begin(Keyboard keyboard)
        {
            this.keyboard = keyboard;
            keyboard.KeyDown += keyboard_KeyDown;
            keyboard.KeyUp += keyboard_KeyUp;
            client.UpdateDispatcher.Register(this);
        }

        public void End()
        {
            keyboard.KeyDown -= keyboard_KeyDown;
            keyboard.KeyUp -= keyboard_KeyUp;
            client.UpdateDispatcher.Hide(this);
        }

        public void Update(GameTime gameTime)
        {
            int i = 0;
            while (i < recentUpList.Count)
            {
                if (recentUpList[i].RemainedTime < gameTime.DeltaTime)
                    recentUpList.RemoveAt(i);
                else
                {
                    recentUpList[i] = new DoubleKeyTime()
                    {
                        Key = recentUpList[i].Key,
                        RemainedTime = recentUpList[i].RemainedTime - gameTime.DeltaTime
                    };
                    i++;
                }
            }
        }

        #region keyboard and mouse handlers

        void keyboard_KeyUp(Keys key)
        {
            packet.Command.Type = InputCommand.CommandType.KeyboardUp;
            packet.Command.KeyboardKey = key;
            client.SendPacket(packet);

            recentUpList.Add(new DoubleKeyTime()
            {
                Key = key,
                RemainedTime = (Second)0.15f
            });
        }

        void keyboard_KeyDown(Keys key)
        {
            packet.Command.Type = InputCommand.CommandType.KeyboardDown;
            packet.Command.KeyboardKey = key;
            client.SendPacket(packet);

            for (int i = 0; i < recentUpList.Count; i++)
            {
                if (recentUpList[i].Key == key)
                {
                    recentUpList.RemoveAt(i);

                    packet.Command.Type = InputCommand.CommandType.KeyboardDouble;
                    client.SendPacket(packet);
                }
            }
        }

        #endregion

        #region privates

        Packet.ClientToServer.SendCommand packet;
        Client client;
        Keyboard keyboard;

        struct DoubleKeyTime
        {
            public Keys Key;
            public Second RemainedTime;

            public void Update(GameTime gameTime)
            {
                RemainedTime -= gameTime.DeltaTime;
            }
        }
        List<DoubleKeyTime> recentUpList;

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAJE.Cosmos.Network.Local
{
    public class InputDispatcher : IUpdatable
    {
        public InputDispatcher(Server server, int playerCount)
        {
            this.server = server;
            this.sendCommand = new Packet.ClientToServer.SendCommand();
            this.frameInput = new Packet.ServerToClient.FrameInput(playerCount);
            this.sendCommand.OnReceived += sendCommand_OnReceived;
            this.playerCount = playerCount;
        }

        public void Begin()
        {
            server.PacketDispatcher.Register(sendCommand);
            server.UpdateDispatcher.Register(this);
            frameInput.Frame.Initialize(0);
            frameTimeRemains = (Second)0;
            SendFrameInputBuffer();
        }

        public void End()
        {
            server.PacketDispatcher.Hide(sendCommand);
            server.UpdateDispatcher.Hide(this);
        }

        public void Update(GameTime gameTime)
        {
            frameTimeRemains -= gameTime.DeltaTime;
            if (frameTimeRemains < 0)
            {
                frameTimeRemains += FrameInput.DefaultFrameInterval;
                SendFrameInputBuffer();

                // 서버에 렉 발생시 갑작스러운 빠른 게임 재생은 피해야 한다.
                if (frameTimeRemains < 0)
                    frameTimeRemains = FrameInput.DefaultFrameInterval;
            }
        }

        #region privates

        void SendFrameInputBuffer()
        {
            server.SendPacket(frameInput);
            frameInput.Frame.Initialize(frameInput.Frame.FrameIndex + 1);
        }

        void sendCommand_OnReceived()
        {
            int cIndex = server.GetRecentlyReceivedMessageConnectionIndex();
            if(cIndex < 0) return;
            frameInput.Frame.Inputs[cIndex].CommandList.Add(sendCommand.Command);
        }

        Server server;
        Packet.ClientToServer.SendCommand sendCommand;
        Packet.ServerToClient.FrameInput frameInput;
        int playerCount;
        Second frameTimeRemains;

        #endregion
    }
}

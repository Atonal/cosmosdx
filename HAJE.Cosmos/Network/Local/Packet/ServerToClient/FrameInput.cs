﻿using Lidgren.Network;

namespace HAJE.Cosmos.Network.Local.Packet.ServerToClient
{
    public class FrameInput : PacketBase
    {
        public Local.FrameInput Frame;

        protected override void WriteFields(NetOutgoingMessage msg)
        {
            msg.Write(Frame.FrameIndex);
            msg.Write(Frame.FrameLength);
            for (int i = 0; i < Frame.Inputs.Length; i++)
            {
                PlayerInput input = Frame.Inputs[i];
                msg.Write(input.MouseX);
                msg.Write(input.MouseY);
                msg.Write((byte)input.CommandList.Count);
                for (int j = 0; j < input.CommandList.Count; j++)
                {
                    msg.Write(input.CommandList[j]);
                }
            }
        }

        protected override void ReadFields(NetIncomingMessage msg)
        {
            Frame.FrameIndex = msg.ReadInt32();
            Frame.FrameLength = (Second)msg.ReadSingle();
            for (int i = 0; i < Frame.Inputs.Length; i++)
            {
                Frame.Inputs[i].MouseX = msg.ReadSingle();
                Frame.Inputs[i].MouseY = msg.ReadSingle();
                int count = msg.ReadByte();
                Frame.Inputs[i].CommandList.Clear();
                for (int j = 0; j < count; j++)
                {
                    Frame.Inputs[i].CommandList.Add(msg.ReadInputCommand());
                }
            }
        }

        public FrameInput(int playerCount)
            : base((int)PacketCode.FrameInput)
        {
            Frame = new Local.FrameInput(playerCount);
        }
    }
}

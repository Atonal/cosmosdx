﻿using Lidgren.Network;

namespace HAJE.Cosmos.Network.Local.Packet
{
    public static class PacketBuildHelper
    {
        public static void Write(this NetOutgoingMessage msg, InputCommand cmd)
        {
            msg.Write((byte)cmd.Type);
            msg.Write(cmd.CommandParameter);
        }

        public static InputCommand ReadInputCommand(this NetIncomingMessage msg)
        {
            InputCommand cmd = new InputCommand();
            cmd.Type = (InputCommand.CommandType)msg.ReadByte();
            cmd.CommandParameter = msg.ReadByte();
            return cmd;
        }
    }
}

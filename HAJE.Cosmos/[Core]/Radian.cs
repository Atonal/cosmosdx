﻿using SharpDX;
using System;

namespace HAJE.Cosmos
{
    /// <summary>
    /// 각도의 단위를 명시하기 위한 타입.
    /// 각도를 호도법 단밀도 부동소숫점으로 표현한다.
    /// 부동소숫점이나 육십분법과의 형 변환을 지원한다.
    /// </summary>
    public struct Radian
    {
        public Radian(float radian)
        {
            value = radian;
        }
        public Radian(Degree degree)
        {
            value = (float)degree / 180.0f * Pi;
        }

        public static float Cos(Radian r)
        {
            return (float)Math.Cos(r.value);
        }
        public static float Sin(Radian r)
        {
            return (float)Math.Sin(r.value);
        }
        public static float Tan(Radian r)
        {
            return (float)Math.Tan(r.value);
        }

        public Vector2 Rotate(Vector2 v)
        {
            return new Vector2(
                (float)(v.X * Math.Cos(value) - v.Y * Math.Sin(value)),
                (float)(v.X * Math.Sin(value) + v.Y * Math.Cos(value))
            );
        }

        public static explicit operator Radian(float radian)
        {
            return new Radian(radian);
        }

        public static implicit operator Radian(Degree degree)
        {
            return new Radian(degree);
        }

        public static implicit operator float(Radian radian)
        {
            return radian.value;
        }

        public static Radian operator +(Radian a, Radian b)
        {
            return new Radian(a.value + b.value);
        }

        public static Radian operator -(Radian a, Radian b)
        {
            return new Radian(a.value - b.value);
        }

        public static Radian operator /(Radian a, float f)
        {
            return new Radian(a.value / f);
        }

        public static Radian operator *(Radian a, float f)
        {
            return new Radian(a.value * f);
        }

        public static Radian operator *(float f, Radian a)
        {
            return a * f;
        }

        public static readonly Radian Pi = new Radian((float)Math.PI);

        private float value;
    }
}

﻿using HAJE.Cosmos.Rendering.VertexTypes;
using SharpDX;
using SharpDX.Direct3D9;
using System;

namespace HAJE.Cosmos.Combat.Background
{
    public class NebulaRenderer : IDisposable
    {
        public const float NebulaSizeScale = 100.0f;

        public NebulaRenderer(Device graphicsDevice)
        {
            this.graphicsDevice = graphicsDevice;
            this.noiseTexture = Texture.FromFile(
                GameSystem.GraphicsDevice,
                "Resource/Background/NebulaNoise.png",
                SharpDX.Direct3D9.Usage.None,
                SharpDX.Direct3D9.Pool.Managed
            );
        }

        public void BuildMesh(NebularData[] nebulas)
        {
            nebulaData = new NebularData[nebulas.Length];
            for (int i = 0; i < nebulas.Length; i++)
                nebulaData[i] = nebulas[i];

            if (vertexBuffer != null)
                vertexBuffer.Dispose();
            if (indexBuffer != null)
                indexBuffer.Dispose();

            vertexBuffer = new VertexBuffer(
                graphicsDevice,
                Rendering.BillboardBuilder.GetVertexBufferSize(nebulas.Length),
                Usage.WriteOnly,
                VertexFormat.None,
                Pool.Managed
            );
            using (DataStream buffer = vertexBuffer.Lock(
                0,
                Rendering.BillboardBuilder.GetVertexBufferSize(nebulas.Length),
                LockFlags.Discard))
            {
                for (int i = 0; i < nebulas.Length; i++)
                {
                    Rendering.BillboardBuilder.WriteVertexBuffer(
                        nebulas[i].NoiseIndex.TextureUV,
                        new Color(nebulas[i].Color),
                        new Vector4(nebulas[i].Position, 1),
                        new Vector2(0.5f, 0.5f),
                        nebulas[i].Size * NebulaSizeScale,
                        nebulas[i].Rotation,
                        buffer
                    );
                }
                vertexBuffer.Unlock();
            }

            indexBuffer = new IndexBuffer(
                graphicsDevice,
                Rendering.BillboardBuilder.GetIndexBufferSize(nebulas.Length),
                Usage.WriteOnly,
                Pool.Managed,
                false
            );
            using (DataStream buffer = indexBuffer.Lock(
                0,
                Rendering.BillboardBuilder.GetIndexBufferSize(nebulas.Length),
                LockFlags.Discard))
            {
                Rendering.BillboardBuilder.WriteIndexBuffer(nebulas.Length, buffer);
                indexBuffer.Unlock();
            }
        }

        public void Render(Rendering.Camera camera)
        {
            if (vertexBuffer == null)
                return;

            graphicsDevice.SetTexture(0, noiseTexture);
            graphicsDevice.SetRenderState(RenderState.Lighting, false);
            graphicsDevice.SetRenderState(RenderState.ZEnable, false);
            graphicsDevice.SetRenderState(RenderState.AlphaBlendEnable, true);
            graphicsDevice.SetRenderState(RenderState.BlendOperation, BlendOperation.Add);
            graphicsDevice.SetRenderState(RenderState.SourceBlend, Blend.One);
            graphicsDevice.SetRenderState(RenderState.DestinationBlend, Blend.One);
            graphicsDevice.SetStreamSource(0, vertexBuffer, 0, PositionColorTextureVertexType.Size);
            graphicsDevice.Indices = indexBuffer;
            graphicsDevice.SetTransform(0, camera.TransformMatrix);
            graphicsDevice.VertexDeclaration = PositionColorTextureVertexType.VertexDeclaration;

            graphicsDevice.DrawIndexedPrimitive(
                PrimitiveType.TriangleList, 0, 0,
                Rendering.BillboardBuilder.GetVertexCount(nebulaData.Length),
                0,
                Rendering.BillboardBuilder.GetPrimitiveCount(nebulaData.Length)
            );
        }

        public void Dispose()
        {
            if (vertexBuffer != null) vertexBuffer.Dispose(); vertexBuffer = null;
            if (indexBuffer != null) indexBuffer.Dispose(); indexBuffer = null;
            if (noiseTexture != null) noiseTexture.Dispose(); noiseTexture = null;
        }

        private Device graphicsDevice;
        private VertexBuffer vertexBuffer;
        private IndexBuffer indexBuffer;
        private Texture noiseTexture;
        private NebularData[] nebulaData;
    }
}

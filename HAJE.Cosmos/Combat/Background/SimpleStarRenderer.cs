﻿using HAJE.Cosmos.Rendering.VertexTypes;
using SharpDX;
using SharpDX.Direct3D9;
using System;

namespace HAJE.Cosmos.Combat.Background
{
    public class SimpleStarRenderer : IDisposable
    {
        public SimpleStarRenderer(Device graphicsDevice)
        {
            this.device = graphicsDevice;

            this.effect = Effect.FromFile(graphicsDevice,
                "Resource/Background/SpriteStar.fx",
                ShaderFlags.None
            );
            technique = effect.GetTechnique(0);
            effect.Technique = technique;
        }

        public void SetDotStar(SimpleStarData[] stars)
        {
            if (dotStarVertexBuffer != null)
                dotStarVertexBuffer.Dispose();

            dotStarVertexBuffer = new VertexBuffer(
                device,
                stars.Length * PositionColorVertexType.Size,
                Usage.WriteOnly,
                VertexFormat.None,
                Pool.Managed
            );

            PositionColorVertexType[] vertices = new PositionColorVertexType[stars.Length];
            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i].Color = new Color(stars[i].Color);
                vertices[i].Position = stars[i].Position;
            }

            using (DataStream buffer = dotStarVertexBuffer.Lock(0, 0, LockFlags.None))
            {
                for (int i = 0; i < vertices.Length; i++)
                    vertices[i].WriteTo(buffer);
                dotStarVertexBuffer.Unlock();
            }

            dotStarCount = vertices.Length;
        }

        public void SetSpriteStar(SimpleStarData[] stars)
        {
            if (spriteStarVertexBuffer != null)
                spriteStarVertexBuffer.Dispose();
            if (spriteStarIndexBuffer != null)
                spriteStarIndexBuffer.Dispose();

            spriteStarCount = stars.Length;
            spriteStarVertexBuffer = new VertexBuffer(
                device,
                Rendering.BillboardBuilder.GetVertexBufferSize(spriteStarCount),
                Usage.WriteOnly,
                VertexFormat.None,
                Pool.Managed
            );
            using (DataStream buffer = spriteStarVertexBuffer.Lock(0, 0, LockFlags.None))
            {
                for (int i = 0; i < spriteStarCount; i++)
                    Rendering.BillboardBuilder.WriteVertexBuffer(
                        new Color(stars[i].Color),
                        new Vector4(stars[i].Position, 1),
                        buffer
                    );
                spriteStarVertexBuffer.Unlock();
            }

            spriteStarIndexBuffer = new IndexBuffer(
                device,
                Rendering.BillboardBuilder.GetIndexBufferSize(spriteStarCount),
                Usage.WriteOnly,
                Pool.Managed,
                false
            );
            using (DataStream buffer = spriteStarIndexBuffer.Lock(0, 0, LockFlags.None))
            {
                Rendering.BillboardBuilder.WriteIndexBuffer(spriteStarCount, buffer);
                spriteStarIndexBuffer.Unlock();
            }
        }

        public void Render(Rendering.Camera camera)
        {
            if (dotStarVertexBuffer == null) return;

            device.SetTexture(0, null);
            device.SetRenderState(RenderState.Lighting, false);
            device.SetRenderState(RenderState.ZEnable, false);
            device.SetRenderState(RenderState.AlphaBlendEnable, true);
            device.SetRenderState(RenderState.BlendOperation, BlendOperation.Add);
            device.SetRenderState(RenderState.SourceBlend, Blend.One);
            device.SetRenderState(RenderState.DestinationBlend, Blend.One);

            // 점 파티클 기반 별
            device.SetTransform(0, camera.TransformMatrix);
            device.SetStreamSource(0, dotStarVertexBuffer, 0, PositionColorVertexType.Size);
            device.VertexDeclaration = PositionColorVertexType.VertexDeclaration;
            device.DrawPrimitives(PrimitiveType.PointList, 0, dotStarCount);

            // 빌보드 기반 별
            device.SetStreamSource(0, spriteStarVertexBuffer, 0, PositionColorTextureVertexType.Size);
            device.Indices = spriteStarIndexBuffer;
            device.SetTransform(0, Matrix.Identity);
            device.VertexDeclaration = PositionColorTextureVertexType.VertexDeclaration;
            effect.Begin();
            effect.BeginPass(0);
            effect.SetValue("transform", camera.TransformMatrix);
            effect.SetValue("viewportSize", new Vector4(device.Viewport.Width, device.Viewport.Height, 0, 0));
            device.DrawIndexedPrimitive(
                PrimitiveType.TriangleList, 0, 0,
                Rendering.BillboardBuilder.GetVertexCount(spriteStarCount),
                0,
                Rendering.BillboardBuilder.GetPrimitiveCount(spriteStarCount)
            );
            effect.EndPass();
            effect.End();
        }

        public void Dispose()
        {
            if (dotStarVertexBuffer != null) dotStarVertexBuffer.Dispose(); dotStarVertexBuffer = null;
            if (effect != null) effect.Dispose(); effect = null;
            if (spriteStarVertexBuffer != null) spriteStarVertexBuffer.Dispose(); spriteStarVertexBuffer = null;
            if (spriteStarIndexBuffer != null) spriteStarIndexBuffer.Dispose(); spriteStarIndexBuffer = null;
        }

        private Device device;

        private VertexBuffer dotStarVertexBuffer;
        private int dotStarCount;

        private Effect effect;
        private EffectHandle technique;
        private VertexBuffer spriteStarVertexBuffer;
        private IndexBuffer spriteStarIndexBuffer;
        private int spriteStarCount;
    }
}

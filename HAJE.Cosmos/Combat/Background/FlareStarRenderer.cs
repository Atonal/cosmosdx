﻿using HAJE.Cosmos.Rendering.VertexTypes;
using SharpDX;
using SharpDX.Direct3D9;
using System;

namespace HAJE.Cosmos.Combat.Background
{
    public class FlareStarRenderer : IDisposable
    {
        public const int MaxFlarStarCount = 400;
        public const float FlareSizeScale = 100.0f;
        public const int MaxAnimationGroup = 32;

        public FlareStarRenderer(Device graphicsDevice)
        {
            this.graphicsDevice = graphicsDevice;
            this.sourceVertexBuffer = new VertexBuffer(
                graphicsDevice,
                Rendering.BillboardBuilder.GetVertexBufferSize(MaxFlarStarCount),
                Usage.WriteOnly,
                VertexFormat.None,
                Pool.Default
            );
            this.flareVertexBuffer = new VertexBuffer(
                graphicsDevice,
                Rendering.BillboardBuilder.GetVertexBufferSize(MaxFlarStarCount),
                Usage.WriteOnly,
                VertexFormat.None,
                Pool.Default
            );
            this.indexBuffer = new IndexBuffer(
                graphicsDevice,
                Rendering.BillboardBuilder.GetIndexBufferSize(MaxFlarStarCount),
                Usage.WriteOnly,
                Pool.Default,
                false
            );
            this.lightSourceTexture = Texture.FromFile(
                GameSystem.GraphicsDevice,
                "Resource/Background/LightSource.png",
                SharpDX.Direct3D9.Usage.None,
                SharpDX.Direct3D9.Pool.Managed
            );
            this.flareTexture = Texture.FromFile(
                GameSystem.GraphicsDevice,
                "Resource/Background/Flare.png",
                SharpDX.Direct3D9.Usage.None,
                SharpDX.Direct3D9.Pool.Managed
            );
            this.groupData = new FlareStarAnimationGroupData[MaxAnimationGroup];
            for(int i=0; i<MaxAnimationGroup; i++)
            {
                this.groupData[i].Rotation = new Radian(0);
                this.groupData[i].Scale = 1.0f;
            }

            this.animations = new FlareStarAnimation[MaxAnimationGroup];
            for (int i = 0; i < MaxAnimationGroup; i++)
            {
                this.animations[i] = new FlareStarAnimation(this, i);
            }
        }

        public void SetData(FlareStarData[] stars)
        {
            isIndexDirty = true;
            if (stars.Length > MaxFlarStarCount)
                throw new ArgumentOutOfRangeException("stars", "허용 가능한 최대 천체 수를 넘었습니다.");

            flareData = stars;
        }

        public FlareStarAnimation GetAnimation(int animationGroup)
        {
            if (animationGroup < 0 || animationGroup >= MaxAnimationGroup)
                throw new ArgumentOutOfRangeException("animationGroup", "잘못된 애니메이션 그룹 번호입니다.");

            return animations[animationGroup];
        }

        public void SetAnimationGroupData(int animationGroup, FlareStarAnimationGroupData data)
        {
            if (animationGroup < 0 || animationGroup >= MaxAnimationGroup)
                throw new ArgumentOutOfRangeException("animationGroup", "잘못된 애니메이션 그룹 번호입니다.");

            groupData[animationGroup] = data;
        }
        
        public void RenderSource(Second deltaTime, Rendering.Camera camera)
        {
            if (flareData == null) return;

            for (int i = 0; i < animations.Length; i++)
                animations[i].Update(deltaTime);

            BuildIndexBuffer();

            using (DataStream buffer = sourceVertexBuffer.Lock(
                0,
                Rendering.BillboardBuilder.GetVertexBufferSize(flareData.Length),
                LockFlags.Discard))
            {
                for(int i=0; i<flareData.Length; i++)
                {
                    FlareStarData d = flareData[i];
                    FlareStarAnimationGroupData a = groupData[d.AnimationGroup];
                    Rendering.BillboardBuilder.WriteVertexBuffer(
                        null,   
                        Color.White,   // 광원은 무조건 흰색으로 색상을 입히지 않는다.
                        new Vector4(d.Position, 1),
                        new Vector2(0.5f, 0.5f),
                        new Vector2(d.LightSourceRadius * FlareSizeScale * a.Scale),
                        GetFlareAngleFromCamera(camera) + a.Rotation,
                        buffer
                    );
                }
                sourceVertexBuffer.Unlock();
            }

            graphicsDevice.SetTexture(0, lightSourceTexture);
            graphicsDevice.SetRenderState(RenderState.Lighting, false);
            graphicsDevice.SetRenderState(RenderState.ZEnable, false);
            graphicsDevice.SetRenderState(RenderState.AlphaBlendEnable, true);
            graphicsDevice.SetSamplerState(0, SamplerState.MagFilter, TextureFilter.Linear);
            graphicsDevice.SetSamplerState(0, SamplerState.MinFilter, TextureFilter.Linear);
            graphicsDevice.SetSamplerState(0, SamplerState.MipFilter, TextureFilter.Linear);
            graphicsDevice.SetRenderState(RenderState.BlendOperation, BlendOperation.Add);
            graphicsDevice.SetRenderState(RenderState.SourceBlend, Blend.One);
            graphicsDevice.SetRenderState(RenderState.DestinationBlend, Blend.One);
            graphicsDevice.SetStreamSource(0, sourceVertexBuffer, 0, PositionColorTextureVertexType.Size);
            graphicsDevice.Indices = indexBuffer;
            graphicsDevice.SetTransform(0, camera.TransformMatrix);
            graphicsDevice.VertexDeclaration = PositionColorTextureVertexType.VertexDeclaration;

            graphicsDevice.DrawIndexedPrimitive(
                PrimitiveType.TriangleList, 0, 0,
                Rendering.BillboardBuilder.GetVertexCount(flareData.Length),
                0,
                Rendering.BillboardBuilder.GetPrimitiveCount(flareData.Length)
            );
        }

        public void RenderFlare(Rendering.Camera camera)
        {
            if (flareData == null) return;

            BuildIndexBuffer();

            using (DataStream buffer = sourceVertexBuffer.Lock(
                0,
                Rendering.BillboardBuilder.GetVertexBufferSize(flareData.Length),
                LockFlags.Discard))
            {
                for (int i = 0; i < flareData.Length; i++)
                {
                    FlareStarData d = flareData[i];
                    FlareStarAnimationGroupData a = groupData[d.AnimationGroup];
                    Rendering.BillboardBuilder.WriteVertexBuffer(
                        null,
                        new Color(flareData[i].Color),
                        new Vector4(flareData[i].Position, 1),
                        new Vector2(0.5f, 0.5f),
                        new Vector2(flareData[i].FlareRadius * FlareSizeScale * a.Scale),
                        GetFlareAngleFromCamera(camera) + a.Rotation,
                        buffer
                    );
                }
                sourceVertexBuffer.Unlock();
            }

            graphicsDevice.SetTexture(0, flareTexture);
            graphicsDevice.SetRenderState(RenderState.Lighting, false);
            graphicsDevice.SetRenderState(RenderState.ZEnable, false);
            graphicsDevice.SetRenderState(RenderState.AlphaBlendEnable, true);
            graphicsDevice.SetRenderState(RenderState.BlendOperation, BlendOperation.Add);
            graphicsDevice.SetRenderState(RenderState.SourceBlend, Blend.One);
            graphicsDevice.SetRenderState(RenderState.DestinationBlend, Blend.One);
            graphicsDevice.SetStreamSource(0, sourceVertexBuffer, 0, PositionColorTextureVertexType.Size);
            graphicsDevice.Indices = indexBuffer;
            graphicsDevice.SetTransform(0, camera.TransformMatrix);
            graphicsDevice.VertexDeclaration = PositionColorTextureVertexType.VertexDeclaration;

            graphicsDevice.DrawIndexedPrimitive(
                PrimitiveType.TriangleList, 0, 0,
                Rendering.BillboardBuilder.GetVertexCount(flareData.Length),
                0,
                Rendering.BillboardBuilder.GetPrimitiveCount(flareData.Length)
            );
        }

        public void Dispose()
        {
            if (sourceVertexBuffer != null) sourceVertexBuffer.Dispose(); sourceVertexBuffer = null;
            if (flareVertexBuffer != null) flareVertexBuffer.Dispose(); flareVertexBuffer = null;
            if (indexBuffer != null) indexBuffer.Dispose(); indexBuffer = null;
            if (lightSourceTexture != null) lightSourceTexture.Dispose(); lightSourceTexture = null;
            if (flareTexture != null) flareTexture.Dispose(); flareTexture = null;
        }

        private void BuildIndexBuffer()
        {
            if (!isIndexDirty) return;

            using (DataStream buffer = indexBuffer.Lock(
                0,
                Rendering.BillboardBuilder.GetIndexBufferSize(flareData.Length),
                LockFlags.Discard))
            {
                Rendering.BillboardBuilder.WriteIndexBuffer(flareData.Length, buffer);
                indexBuffer.Unlock();
            }
        }

        private Radian GetFlareAngleFromCamera(Rendering.Camera camera)
        {
            Vector2 pos = camera.Position;
            return new Radian((pos.X - pos.Y*0.75f) / 500.0f);
        }

        private Device graphicsDevice;
        private VertexBuffer sourceVertexBuffer;
        private VertexBuffer flareVertexBuffer;
        private IndexBuffer indexBuffer;
        private Texture lightSourceTexture;
        private Texture flareTexture;
        private bool isIndexDirty;

        private FlareStarAnimation[] animations = new FlareStarAnimation[MaxAnimationGroup];
        private FlareStarAnimationGroupData[] groupData;
        private FlareStarData[] flareData;
    }
}

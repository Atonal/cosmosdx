﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.Cosmos.Combat.Background
{
    public struct FlareStarData
    {
        public Vector3 Position;
        public Color3 Color;
        public float LightSourceRadius;
        public float FlareRadius;
        public int AnimationGroup;
    }

    public class FlareStarBuilder
    {
        public FlareStarBuilder()
        {
            SetScaleRange(2.5f, 8.0f);
        }

        public void ClearColorPalette()
        {
            colorPalettes.Clear();
        }
        public void ClearAnimationPalette()
        {
            animationPalette.Clear();
        }

        public void AddColorToPalette(ColorPalette palette, int weight)
        {
            colorPalettes.Add(palette, weight);
        }
        public void AddAnimationGroupPalette(int animationGroup)
        {
            Debug.Assert(0 <= animationGroup && animationGroup < FlareStarRenderer.MaxAnimationGroup);
            animationPalette.Add(animationGroup);
        }

        public void SetSourceScaleRange(float min, float max)
        {
            Debug.Assert(0 < min && min <= max);
            minSourceScale = min;
            maxSourceScale = max;
        }
        public void SetFlareScaleVariation(float min, float max)
        {
            Debug.Assert(0 < min && min <= max);
            minFlareScaleVariation = min;
            maxFlareScaleVariation = max;
        }
        public void SetScaleRange(float min, float max)
        {
            SetSourceScaleRange(min, max);
            SetFlareScaleVariation(1, 1);
        }

        public void Clear()
        {
            innerList.Clear();
        }
        public void AppendUniform(Vector3 point1, Vector3 point2, int count, int seed)
        {
            Debug.Assert(animationPalette.Count > 0);
            Random rand = new Random(seed);

            Vector3 max = Vector3.Max(point1, point2);
            Vector3 min = Vector3.Min(point1, point2);
            for (int i = 0; i < count; i++)
            {
                Append(new Vector3()
                    {
                        X = rand.NextFloat(min.X, max.X),
                        Y = rand.NextFloat(min.Y, max.Y),
                        Z = rand.NextFloat(min.Z, max.Z)
                    },
                    rand
                );
            }
        }
        public void AppendCircular(Vector3 center, Vector3 radius, Vector3 rotation, int count, int seed)
        {
            Debug.Assert(animationPalette.Count > 0);
            Random rand = new Random(seed);

            Matrix rot = Matrix.RotationYawPitchRoll(
                rotation.Y,
                rotation.X,
                rotation.Z
            );
            for (int i = 0; i < count; i++)
            {
                float r = rand.NextFloat(0, 1);
                Radian theta = (Radian)rand.NextFloat(0, Radian.Pi);
                Radian rho = (Radian)rand.NextFloat(0, Radian.Pi * 2);
                Vector4 unrotated = new Vector4(
                    r * radius.X * Radian.Cos(theta) * Radian.Cos(rho),
                    r * radius.Y * Radian.Sin(theta) * Radian.Cos(rho),
                    r * radius.Z * Radian.Sin(rho),
                    0
                );
                Vector4 rotated = Vector4.Transform(unrotated, rot);
                Append(new Vector3()
                    {
                        X = rotated.X + center.X,
                        Y = rotated.Y + center.Y,
                        Z = rotated.Z + center.Z
                    },
                    rand
                );
            }
        }

        private void Append(Vector3 position, Random rand)
        {
            int aniGroupIndex = rand.Next(animationPalette.Count);
            float sourceRadius = rand.NextFloat(
                minSourceScale, maxSourceScale
            );
            float flareRadiusVariation = rand.NextFloat(
                minFlareScaleVariation, maxFlareScaleVariation
            );
            float flareRadius = flareRadiusVariation * sourceRadius;

            innerList.Add(new FlareStarData()
                {
                    AnimationGroup = animationPalette[aniGroupIndex],
                    Color = colorPalettes.GetRandom(rand).GetColor(rand),
                    FlareRadius = flareRadius,
                    LightSourceRadius = sourceRadius,
                    Position = position
                }
            );
        }

        public void Append(FlareStarData data)
        {
            innerList.Add(data);
        }

        public Color3 GetPaletteColor(int seed)
        {
            Random rand = new Random(seed);
            return colorPalettes.GetRandom(rand).GetColor(rand);
        }

        public FlareStarData[] ToArray()
        {
            return innerList.ToArray();
        }

        private List<FlareStarData> innerList = new List<FlareStarData>();
        private List<int> animationPalette = new List<int>();
        private Utility.WeightedRandomPool<ColorPalette> colorPalettes = new Utility.WeightedRandomPool<ColorPalette>();
        
        private float minSourceScale;
        private float maxSourceScale;
        private float minFlareScaleVariation;
        private float maxFlareScaleVariation;

        private const int defaultScaleDice = 1;
    }
}

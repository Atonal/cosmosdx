﻿
namespace HAJE.Cosmos.Combat.Background
{
    public interface IBackgroundBuilder
    {
        void Build(BackgroundRenderer background);
    }
}

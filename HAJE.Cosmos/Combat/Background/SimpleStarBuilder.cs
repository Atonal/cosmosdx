﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.Cosmos.Combat.Background
{
    public struct ColorPalette
    {
        public Color3 ColorFrom;
        public Color3 ColorTo;
        public FloatRange ToBlend;    // 0에서 1사이
        public FloatRange Brightness;   // 0에서 1사이

        public Color3 GetColor(Random rand)
        {
            float fromRatio = rand.NextFloat(ToBlend.Min, ToBlend.Max);
            return Color3.Lerp(ColorFrom, ColorTo, fromRatio);
        }

        public float GetBrightness(Random rand)
        {
            return rand.NextFloat(Brightness.Min, Brightness.Max);
        }
    }

    public struct SimpleStarData
    {
        public Vector3 Position;
        public Color3 Color;
    }

    public class SimpleStarBuilder
    {
        public void AddPalette(ColorPalette palette, int weight)
        {
            Debug.Assert(weight > 0);
            palettes.Add(palette, weight);
        }

        public void ClearPalette()
        {
            palettes.Clear();
        }

        public void AddUniformRectangular(float minX, float maxX, float minY, float maxY, float minZ, float maxZ, float density)
        {
            Debug.Assert(!palettes.IsEmpty());

            float area = (maxX - minX) * (maxY - minY);
            int count = (int)(area * density / 10000);
            count = Math.Max(count, 1);

            for (int i = 0; i < count; i++)
            {
                SimpleStarData s;
                s.Position = new Vector3(
                    rand.NextFloat(minX, maxX),
                    rand.NextFloat(minY, maxY),
                    rand.NextFloat(minZ, maxZ));

                var palette = palettes.GetRandom(rand);
                var color = palette.GetColor(rand);
                var brightness = palette.GetBrightness(rand);
                s.Color = color * brightness;
                starData.Add(s);
            }
        }

        public SimpleStarData[] FlushData()
        {
            var ret = starData.ToArray();
            starData.Clear();
            return ret;
        }

        public void InitSeed(int seed)
        {
            rand = new Random(seed);
        }

        #region privates

        Utility.WeightedRandomPool<ColorPalette> palettes = new Utility.WeightedRandomPool<ColorPalette>();
        List<SimpleStarData> starData = new List<SimpleStarData>();

        Random rand = new Random();

        #endregion
    }
}

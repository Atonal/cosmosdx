﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAJE.Cosmos.Combat.Background
{
    public static class ZOrder
    {
        public const float GameDepth = 500.0f;
        public const float ScaleAtGameDepth = 0.4f;

        public static readonly FloatRange DeepSkyDepth = new FloatRange(9000, 11000);
        public const float MinStarDepth = 7500.0f;
        public static readonly FloatRange BackDepth = new FloatRange(1000, 5000);
        public static readonly FloatRange MiddleDepth = new FloatRange(750, 950);
        public static readonly FloatRange FrontDepth = new FloatRange(550, 700);
    }
}

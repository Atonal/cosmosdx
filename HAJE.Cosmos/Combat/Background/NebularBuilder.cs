﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.Cosmos.Combat.Background
{
    public struct NebularNoiseIndex
    {
        public const int IndexPerRow = 4;
        public const int IndexPerColumn = 4;
        public const int MaxNebulaNoiseIndex = 16;

        public NebularNoiseIndex(Random rand)
        {
            index = rand.Next(MaxNebulaNoiseIndex);
        }

        public int Index
        {
            get
            {
                return index;
            }
            set
            {
                if (value >= MaxNebulaNoiseIndex || value < 0)
                    throw new ArgumentOutOfRangeException("value", "허용되지 않는 값입니다.");
                index = value;
            }
        }

        public RectangleF TextureUV
        {
            get
            {
                float width = (1.0f / IndexPerRow);
                float height = (1.0f / IndexPerColumn);
                return new RectangleF(
                    (index / IndexPerRow) * width,
                    (index % IndexPerRow) * height,
                    width,
                    height
                );
            }
        }

        private int index;
    }

    public struct NebularData
    {
        public Vector3 Position;
        public Color3 Color;
        public Vector2 Size;
        public Radian Rotation;
        public NebularNoiseIndex NoiseIndex;
    }

    public class NebularBuilder
    {
        public NebularBuilder()
        {
            MinSize = new Vector2(10, 10);
            MaxSize = new Vector2(50, 50);
        }

        public void ClearColorPalette()
        {
            colorPalette.Clear();
        }

        public void AddColorToPalette(ColorPalette palette, int weight)
        {
            colorPalette.Add(palette, weight);
        }

        public Vector2 MinSize;
        public Vector2 MaxSize;

        public void Clear()
        {
            innerList.Clear();
        }
        public void AppendUniform(Vector3 point1, Vector3 point2, int count, int seed)
        {
            Random rand = new Random(seed);

            Vector3 max = Vector3.Max(point1, point2);
            Vector3 min = Vector3.Min(point1, point2);
            for (int i = 0; i < count; i++)
            {
                Append(new Vector3()
                    {
                        X = rand.NextFloat(min.X, max.X),
                        Y = rand.NextFloat(min.Y, max.Y),
                        Z = rand.NextFloat(min.Z, max.Z)
                    },
                    rand
                );
            }

        }
        public void Append(Vector3 position, Random rand)
        {
            innerList.Add(new NebularData()
                {
                    Color = colorPalette.GetRandom(rand).GetColor(rand),
                    NoiseIndex = new NebularNoiseIndex(rand),
                    Position = position,
                    Rotation = (Radian)rand.NextFloat(Radian.Pi),
                    Size = new Vector2(
                        rand.NextFloat(MinSize.X, MaxSize.X),
                        rand.NextFloat(MinSize.Y, MaxSize.Y)
                    )
                }
            );
        }

        public NebularData[] ToArray()
        {
            return innerList.ToArray();
        }

        private List<NebularData> innerList = new List<NebularData>();
        private Utility.WeightedRandomPool<ColorPalette> colorPalette = new Utility.WeightedRandomPool<ColorPalette>();
    }
}

﻿using HAJE.Cosmos.Combat.Rendering;
using SharpDX.Direct3D9;
using System;

namespace HAJE.Cosmos.Combat.Background
{
    public class BackgroundRenderer : IDisposable
    {
        public BackgroundRenderer(Device graphicsDevice)
        {
            SimpleStarRenderer = new SimpleStarRenderer(graphicsDevice);
            FlareStarRenderer = new FlareStarRenderer(graphicsDevice);
            NebulaRenderer = new Background.NebulaRenderer(graphicsDevice);
            DoodadRenderer = new DoodadRenderer(graphicsDevice);
        }

        public void Draw(GameTime gameTime, Camera camera)
        {
            SimpleStarRenderer.Render(camera);
            NebulaRenderer.Render(camera);
            FlareStarRenderer.RenderSource(gameTime.DeltaTime, camera);
            FlareStarRenderer.RenderFlare(camera);
            DoodadRenderer.Render(camera);
        }

        public void Dispose()
        {
            if (SimpleStarRenderer != null) SimpleStarRenderer.Dispose(); SimpleStarRenderer = null;
            if (FlareStarRenderer != null) FlareStarRenderer.Dispose(); FlareStarRenderer = null;
            if (NebulaRenderer != null) NebulaRenderer.Dispose(); NebulaRenderer = null;
            if (DoodadRenderer != null) DoodadRenderer.Dispose(); DoodadRenderer = null;
        }

        public SimpleStarRenderer SimpleStarRenderer
        {
            get;
            private set;
        }
        public FlareStarRenderer FlareStarRenderer
        {
            get;
            private set;
        }
        public NebulaRenderer NebulaRenderer
        {
            get;
            private set;
        }
        public DoodadRenderer DoodadRenderer
        {
            get;
            private set;
        }
    }
}

﻿using HAJE.Cosmos.Rendering;
using HAJE.Cosmos.Rendering.VertexTypes;
using SharpDX;
using SharpDX.Direct3D9;
using System;
using System.Collections.Generic;
using TextureCache = HAJE.Cosmos.Rendering.TextureCache;
using PiecewiseLinearFunction = HAJE.Cosmos.Utility.PiecewiseLinearFunction;
using System.Diagnostics;

namespace HAJE.Cosmos.Combat.Background
{
    public class DoodadRenderer : IDisposable
    {
        public DoodadRenderer(Device graphicsDevice)
        {
            this.device = graphicsDevice;

            textureCache = new TextureCache(graphicsDevice);
            textureAtlas = new TextureAtlas(graphicsDevice, new Size2(2048, 2048));

            effect = Effect.FromFile(graphicsDevice,
                "Resource/Background/Doodad.fx",
                ShaderFlags.None
            );
            var technique = effect.GetTechnique(0);
            effect.Technique = technique;

            var back = new LayerProperty();
            back.PixelDensity.AddControlPoint(ZOrder.GameDepth, 1.00f);
            back.PixelDensity.AddControlPoint(ZOrder.BackDepth.Min, 0.80f);
            back.PixelDensity.AddControlPoint(ZOrder.BackDepth.Max, 0.25f);
            back.Brightness.AddControlPoint(ZOrder.GameDepth, 1.00f);
            back.Brightness.AddControlPoint(ZOrder.BackDepth.Min, 0.80f);
            back.Brightness.AddControlPoint(ZOrder.BackDepth.Max, 0.25f);
            back.Sharpness.AddControlPoint(ZOrder.GameDepth, 1.00f);
            back.Sharpness.AddControlPoint(ZOrder.BackDepth.Min, 0.85f);
            back.Sharpness.AddControlPoint(ZOrder.BackDepth.Max, 0);
            back.Saturation.AddControlPoint(ZOrder.GameDepth, 1.00f);
            back.Saturation.AddControlPoint(ZOrder.BackDepth.Min, 0.90f);
            back.Saturation.AddControlPoint(ZOrder.BackDepth.Max, 0.50f);
            layerProperties[DoodadLayer.Back] = back;

            var middle = new LayerProperty();
            middle.PixelDensity.AddControlPoint(ZOrder.GameDepth, 1.00f);
            middle.PixelDensity.AddControlPoint(ZOrder.MiddleDepth.Min, 0.80f);
            middle.PixelDensity.AddControlPoint(ZOrder.MiddleDepth.Max, 0.50f);
            middle.PixelDensity.AddControlPoint(ZOrder.MinStarDepth, 0.25f);
            middle.Brightness.AddControlPoint(ZOrder.GameDepth, 1.00f);
            middle.Brightness.AddControlPoint(ZOrder.MiddleDepth.Min, 0.50f);
            middle.Brightness.AddControlPoint(ZOrder.MiddleDepth.Max, 0.25f);
            middle.Brightness.AddControlPoint(ZOrder.MinStarDepth, 0.25f);
            middle.Sharpness.AddControlPoint(ZOrder.GameDepth, 1.00f);
            middle.Sharpness.AddControlPoint(ZOrder.MiddleDepth.Min, 0.75f);
            middle.Sharpness.AddControlPoint(ZOrder.MiddleDepth.Max, 0.25f);
            middle.Sharpness.AddControlPoint(ZOrder.MinStarDepth, 0);
            middle.Saturation.AddControlPoint(ZOrder.GameDepth, 1.00f);
            middle.Saturation.AddControlPoint(ZOrder.MiddleDepth.Min, 0.90f);
            middle.Saturation.AddControlPoint(ZOrder.MiddleDepth.Max, 0.50f);
            middle.Saturation.AddControlPoint(ZOrder.MinStarDepth, 0.25f);
            layerProperties[DoodadLayer.Middle] = middle;

            var front = new LayerProperty();
            front.PixelDensity.AddControlPoint(ZOrder.GameDepth, 1.00f);
            front.PixelDensity.AddControlPoint(ZOrder.FrontDepth.Min, 0.90f);
            front.PixelDensity.AddControlPoint(ZOrder.FrontDepth.Max, 0.80f);
            front.PixelDensity.AddControlPoint(ZOrder.MinStarDepth, 0.25f);
            front.Brightness.AddControlPoint(ZOrder.GameDepth, 1.00f);
            front.Brightness.AddControlPoint(ZOrder.FrontDepth.Min, 0.85f);
            front.Brightness.AddControlPoint(ZOrder.FrontDepth.Max, 0.55f);
            front.Brightness.AddControlPoint(ZOrder.MinStarDepth, 0.25f);
            front.Sharpness.AddControlPoint(ZOrder.GameDepth, 1.00f);
            front.Sharpness.AddControlPoint(ZOrder.FrontDepth.Min, 0.90f);
            front.Sharpness.AddControlPoint(ZOrder.FrontDepth.Max, 0.75f);
            front.Sharpness.AddControlPoint(ZOrder.MinStarDepth, 0);
            front.Saturation.AddControlPoint(ZOrder.GameDepth, 1.00f);
            front.Saturation.AddControlPoint(ZOrder.FrontDepth.Min, 0.90f);
            front.Saturation.AddControlPoint(ZOrder.FrontDepth.Max, 0.50f);
            front.Saturation.AddControlPoint(ZOrder.MinStarDepth, 0.25f);
            layerProperties[DoodadLayer.Front] = front;

            foreach (DoodadLayer layer in Enum.GetValues(typeof(DoodadLayer)))
            {
                Debug.Assert(layerProperties.ContainsKey(layer));
            }

            atlasScaleValue.AddValue(1.0f);
            atlasScaleValue.AddValue(0.75f);
            atlasScaleValue.AddValue(0.66f);
            atlasScaleValue.AddValue(0.5f);
            atlasScaleValue.AddValue(0.33f);
        }

        private void BuildAtlas(DoodadData[] doodads, RectangleF[] allocatedUV, float[] atlasScales)
        {
            for (int i = 0; i < doodadCount; i++)
            {
                var doodad = doodads[i];
                var layerProperty = layerProperties[doodad.Layer];
                var texture = textureCache.GetTexture(doodad.Texture);
                var targetDensity = layerProperty.PixelDensity.GetValueAt(doodad.Position.Z);
                var atlasScale = doodad.Scale * targetDensity;
                atlasScale = atlasScaleValue.Quntize(atlasScale);
                atlasScale = Math.Max(atlasScale, 16.0f / texture.GetSize().Y);

                // 동일한 조건의 텍스쳐가 이미 등록되어 있으면 재활용.
                var alreadyAddedAtlas = atlasList.Find((s) =>
                {
                    return s.SourceTexture == texture
                        && s.AtlasScale == atlasScale;
                });
                if (alreadyAddedAtlas == null)
                {
                    var allocated = textureAtlas.RegisterTexture(
                        texture,
                        (texture.GetSize() * atlasScale).ToSize2(),
                        DoodadAtlasBuildAction);
                    allocatedUV[i] = allocated.UVMap;
                    atlasScales[i] = atlasScale;
                    // 재활용 하지 못한 경우 새로 아틀라스를 추가해서 정보 등록
                    atlasList.Add(new AtlasedStaus()
                    {
                        TargetUV = allocated.UVMap,
                        SourceTexture = texture,
                        AtlasScale = atlasScale
                    });
                }
                else
                {
                    allocatedUV[i] = alreadyAddedAtlas.TargetUV;
                    atlasScales[i] = alreadyAddedAtlas.AtlasScale;
                }
            }

            textureAtlas.BuildAtlas();
        }

        private void DoodadAtlasBuildAction(TextureRenderer renderer, ref TextureFrame source, ref Rectangle area, ref RectangleF atlasUV)
        {
            Vector2 pos = new Vector2(area.Left, area.Top);
            Vector2 scale = new Vector2((float)area.Width / source.Frame.Width, (float)area.Height / source.Frame.Height);
            renderer.DrawTexture(source, null, pos, Vector2.Zero, scale, (Radian)0.0f);
        }

        public void BuildMesh(DoodadData[] doodads)
        {
            if (vertices != null) vertices.Dispose();
            if (indices != null) indices.Dispose();
            textureCache.Clear();

            doodadCount = doodads.Length;
            System.Array.Sort(doodads,
                (d1, d2) => { return (int)(d2.Position.Z - d1.Position.Z); }
            );

            RectangleF[] allocatedUV = new RectangleF[doodads.Length];
            float[] atlasScales = new float[doodads.Length];
            BuildAtlas(doodads, allocatedUV, atlasScales);
            

            vertices = new VertexBuffer(
                device,
                Rendering.BillboardBuilder.GetVertexBufferSize(doodadCount),
                Usage.WriteOnly,
                VertexFormat.None,
                Pool.Managed
            );
            
            using (DataStream buffer = vertices.Lock(0, 0, LockFlags.None))
            {
                var atlasSize = textureAtlas.Size;
                for (int i = 0; i < doodadCount; i++)
                {
                    float scale = doodads[i].Scale;
                    // Z값에 다른 스케일 변화 보존
                    scale = scale * doodads[i].Position.Z / DesignResolution.Height;
                    var layerProperty = layerProperties[doodads[i].Layer];
                    float brightness = layerProperty.Brightness.GetValueAt(doodads[i].Position.Z);
                    float sharpness = layerProperty.Sharpness.GetValueAt(doodads[i].Position.Z);
                    float saturation = layerProperty.Saturation.GetValueAt(doodads[i].Position.Z);
                    Vector2 texSize = new Vector2(
                        atlasSize.Width * allocatedUV[i].Width,
                        atlasSize.Height * allocatedUV[i].Height);
                    Rendering.BillboardBuilder.WriteVertexBuffer(allocatedUV[i],
                        new Color(new Vector3(brightness, sharpness, saturation)),
                        new Vector4(doodads[i].Position, 1),
                        new Vector2(0.5f, 0.5f),
                        texSize * scale / atlasScales[i],
                        doodads[i].Rotation,
                        buffer);
                }
                vertices.Unlock();
            }

            indices = new IndexBuffer(
                device,
                Rendering.BillboardBuilder.GetIndexBufferSize(doodadCount),
                Usage.WriteOnly,
                Pool.Managed,
                false);
            using (DataStream buffer = indices.Lock(0, 0, LockFlags.None))
            {
                Rendering.BillboardBuilder.WriteIndexBuffer(doodadCount, buffer);
                indices.Unlock();
            }
        }

        public void Render(Rendering.Camera camera)
        {
            device.SetTexture(0, textureAtlas.Texutre);
            device.SetRenderState(RenderState.Lighting, false);
            device.SetRenderState(RenderState.ZEnable, false);
            device.SetSamplerState(0, SamplerState.MagFilter, TextureFilter.Linear);
            device.SetSamplerState(0, SamplerState.MinFilter, TextureFilter.Linear);
            device.SetSamplerState(0, SamplerState.MipFilter, TextureFilter.Linear);
            device.SetSamplerState(0, SamplerState.AddressU, TextureAddress.Clamp);
            device.SetSamplerState(0, SamplerState.AddressV, TextureAddress.Clamp);
            device.SetRenderState(RenderState.AlphaBlendEnable, true);
            device.SetRenderState(RenderState.SourceBlend, Blend.SourceAlpha);
            device.SetRenderState(RenderState.DestinationBlend, Blend.InverseSourceAlpha);
            device.SetStreamSource(0, vertices, 0, PositionColorTextureVertexType.Size);
            device.VertexDeclaration = PositionColorTextureVertexType.VertexDeclaration;
            device.Indices = indices;

            effect.Begin();
            effect.BeginPass(0);
            effect.SetValue("transform", camera.TransformMatrix);
            effect.SetValue("textureSize", textureAtlas.Texutre.GetSize());
            device.DrawIndexedPrimitive(
                PrimitiveType.TriangleList, 0, 0,
                Rendering.BillboardBuilder.GetVertexCount(doodadCount),
                0,
                Rendering.BillboardBuilder.GetPrimitiveCount(doodadCount)
            );
            effect.EndPass();
            effect.End();
        }

        public void Dispose()
        {
            if (vertices != null) vertices.Dispose(); vertices = null;
            if (indices != null) indices.Dispose(); indices = null;
            if (textureAtlas != null) textureAtlas.Dispose(); textureAtlas = null;
            if (textureCache != null) textureCache.Dispose(); textureCache = null;
            if (effect != null) effect.Dispose(); effect = null;
        }

        class AtlasedStaus
        {
            public Texture SourceTexture;
            public RectangleF TargetUV;
            public float AtlasScale;
        };
        private List<AtlasedStaus> atlasList = new List<AtlasedStaus>();

        class LayerProperty
        {
            public PiecewiseLinearFunction PixelDensity = new PiecewiseLinearFunction();
            public PiecewiseLinearFunction Brightness = new PiecewiseLinearFunction();
            public PiecewiseLinearFunction Sharpness = new PiecewiseLinearFunction();
            public PiecewiseLinearFunction Saturation = new PiecewiseLinearFunction();
        };
        Dictionary<DoodadLayer, LayerProperty> layerProperties = new Dictionary<DoodadLayer, LayerProperty>();

        private Device device;
        private TextureCache textureCache;
        private Effect effect;
        private VertexBuffer vertices;
        private IndexBuffer indices;
        private TextureAtlas textureAtlas;
        private Utility.Quantizer atlasScaleValue = new Utility.Quantizer();
        private int doodadCount;
    }
}

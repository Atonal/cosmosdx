﻿using SharpDX;

namespace HAJE.Cosmos.Combat.Background
{
    public class StarColor
    {
        public StarColor(string stellarClass, int r, int g, int b, int K)
        {
            this.StellarClass = stellarClass;
            this.Color3 = new Color3(
                r / 255.0f,
                g / 255.0f,
                b / 255.0f
            );
            this.Color = new Color(Color3);
            this.K = K;
        }

        public readonly string StellarClass;
        public readonly Color3 Color3;
        public readonly Color Color;
        public readonly int K;

        public static readonly StarColor DarkRed = new StarColor("T6", 64, 0, 0, 970);
        public static readonly StarColor Red = new StarColor("M9", 240, 0, 0, 2560);
        public static readonly StarColor Orange = new StarColor("M5", 255, 138, 56, 3120);
        public static readonly StarColor BrightOrange = new StarColor("K0", 255, 233, 203, 5250);
        public static readonly StarColor White = new StarColor("G5", 255, 246, 233, 5770);
        public static readonly StarColor BrightBlue = new StarColor("G2", 248, 249, 255, 5860);
        public static readonly StarColor SkyBlue = new StarColor("F0", 177, 204, 255, 7200);
        public static readonly StarColor Blue = new StarColor("B0", 68, 114, 255, 26000);
        public static readonly StarColor DarkBlue = new StarColor("O3", 2, 71, 254, 50000);
    }
}

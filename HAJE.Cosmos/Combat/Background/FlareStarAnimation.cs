﻿using System;

namespace HAJE.Cosmos.Combat.Background
{
    public struct FlareStarAnimationGroupData
    {
        public Radian Rotation;
        public float Scale;
    }

    public class FlareStarAnimation
    {
        public FlareStarAnimation(FlareStarRenderer renderer, int animationGroup)
        {
            this.renderer = renderer;
            this.animationGroup = animationGroup;
            this.angularSpeed = (Radian)0;
            this.minScale = 1.0f;
            this.maxScale = 1.0f;
            this.scaleInterval = 1.0f;
            this.currentAngle = (Radian)0;
            this.currentScale = 0;
        }

        public void SetAngularAnimation(Radian speed, float offset)
        {
            angularSpeed = speed;
            currentAngle = speed * offset;
        }

        public void SetScaleAnimation(float min, float max, float interval, float offset)
        {
            minScale = min;
            maxScale = max;
            currentScaleOffset = offset;
            scaleInterval = interval;
        }

        public void Update(Second deltaTime)
        {
            currentAngle += angularSpeed * deltaTime;

            currentScaleOffset += deltaTime;
            while (currentScaleOffset > scaleInterval)
                currentScaleOffset -= scaleInterval;
            currentScale = LinearScale(minScale, maxScale, scaleInterval, ref currentScaleOffset);
            renderer.SetAnimationGroupData(animationGroup, new FlareStarAnimationGroupData()
            {
                Rotation = currentAngle,
                Scale = currentScale
            });
        }

        private float LinearScale(float min, float max, float interval, ref float offset)
        {
            while (offset > interval)
                offset -= interval;
            float scale = offset / interval;
            if (scale < 0.5f)
                scale = scale * 2;
            else
                scale = (1.0f - scale) * 2;

            return scale * (max - min) + min;
        }


        private FlareStarRenderer renderer;
        private int animationGroup;

        private Radian angularSpeed;
        private float minScale;
        private float maxScale;
        private float scaleInterval;
        
        private Radian currentAngle;
        private float currentScaleOffset;
        private float currentScale;
    }
}

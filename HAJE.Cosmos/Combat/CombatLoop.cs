﻿using CSScriptLibrary;
using HAJE.Cosmos.Combat.Background;
using HAJE.Cosmos.Combat.Rendering;
using SharpDX;
using SharpDX.Direct3D9;
using System;

namespace HAJE.Cosmos.Combat
{
    /// <summary>
    /// 게임의 전투를 실제로 구현하는 곳.
    /// 동기화 된 입력처리 등은 다른 곳에서 처리해 준다고 가정한다.
    /// </summary>
    public class CombatLoop : IDisposable
    {
        public CombatLoop()
        {
        }

        public void Initialize()
        {
            ReloadBackground();

            camera = new Camera(GameSystem.GraphicsDevice.Viewport);
            cameraPosition = Vector2.Zero;
        }

        void ReloadBackground()
        {
            if (background != null) background.Dispose();
            background = new BackgroundRenderer(GameSystem.GraphicsDevice);

            try
            {
                var bgBuilder = CSScript.Evaluator.LoadFile<IBackgroundBuilder>("Resource/Background/SampleBackground.cs");
                bgBuilder.Build(background);
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.ToString());
                CSScript.Evaluator.Reset();
            }
        }

        public void UpdateLogic(GameTime gameTime)
        {
            using (var probe = GameSystem.Profiler.GetProbe("logic update"))
            {
                prevCameraPosition = cameraPosition;
                float speed = 250;
                float deltaPos = speed * gameTime.DeltaTime;
                if (CombatSystem.InputStates[0].IsKeyPressed(Input.Keys.A))
                    cameraPosition = cameraPosition + deltaPos * new Vector2(-1, 0);
                if (CombatSystem.InputStates[0].IsKeyPressed(Input.Keys.D))
                    cameraPosition = cameraPosition + deltaPos * new Vector2(1, 0);
                if (CombatSystem.InputStates[0].IsKeyPressed(Input.Keys.W))
                    cameraPosition = cameraPosition + deltaPos * new Vector2(0, 1);
                if (CombatSystem.InputStates[0].IsKeyPressed(Input.Keys.S))
                    cameraPosition = cameraPosition + deltaPos * new Vector2(0, -1);

                if (CombatSystem.InputStates[0].IsKeyUp(Input.Keys.F5))
                    ReloadBackground();

                GameSystem.DebugOutput.SetWatch("Camera Position", string.Format("({0}, {1})", 
                    (int)cameraPosition.X, (int)cameraPosition.Y));
            }
        }

        public void Draw(GameTime gameTime, float frameAlpha)
        {
            using (var probe = GameSystem.Profiler.GetProbe("rendering"))
            {
                Vector2 pos = new Vector2(
                    GameSystem.GraphicsDevice.Viewport.Width,
                    GameSystem.GraphicsDevice.Viewport.Height) / 2;
                pos.X -= camera.Position.X;
                pos.Y += camera.Position.Y;
                camera.MoveTo(Vector2.Lerp(prevCameraPosition, cameraPosition, frameAlpha));

                GameSystem.GraphicsDevice.BeginScene();
                GameSystem.GraphicsDevice.Clear(ClearFlags.Target | ClearFlags.ZBuffer, new ColorBGRA(0, 0, 0, 0), 1.0f, 0);
                background.Draw(gameTime, camera);
                GameSystem.DebugOutput.Draw();
                GameSystem.GraphicsDevice.EndScene();
                GameSystem.GraphicsDevice.Present();
            }
        }

        public void Dispose()
        {
            if (background != null) background.Dispose(); background = null;
            CombatSystem = null;
        }

        public CombatSystem CombatSystem;
        BackgroundRenderer background;
        Camera camera;
        Vector2 cameraPosition;
        Vector2 prevCameraPosition;
    }
}

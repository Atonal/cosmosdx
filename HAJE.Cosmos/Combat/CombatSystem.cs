﻿using HAJE.Cosmos.Network.Local;
using System.Collections.Generic;

namespace HAJE.Cosmos.Combat
{
    /// <summary>
    /// 입력 동기화 된 게임 루프를 만드는 더러운 코드를 다른 곳에서 숨기기 위해 몰아놓은 곳.
    /// 여기에 전투 시스템을 위한 하부 시스템을 구현하고, 
    /// 좀 더 게임에 가까운 로직은 다른 곳에 나눠 구현한다.
    /// </summary>
    public class CombatSystem : IUpdatable
    {
        public const int LogicUpdatePerFrame = 15;
        public readonly Second LogicUpdateInterval = (Second)(1.0f / LogicUpdatePerFrame);

        public CombatSystem(GameSession session, CombatLoop combatLoop)
        {
            this.session = session;
            inputSender = new InputSender(session.Client);
            inputReceiver = new InputReceiver(session.Client, session.MaxPlayer);
            if (session.IsOwner)
            {
                inputDispatcher = new InputDispatcher(session.Server, session.MaxPlayer);
            }

            inputStates = new PlayerInputState[session.MaxPlayer];
            for (int i = 0; i < session.MaxPlayer; i++)
                inputStates[i] = new PlayerInputState();

            this.combatLoop = combatLoop;
        }

        public void Begin()
        {
            inputSender.Begin(GameSystem.Keyboard);
            inputDispatcher.Begin();
            inputReceiver.Begin();
            GameSystem.UpdateLoop.Register(this);

            logicGameTime = new GameTime();
        }

        public void End()
        {
            inputSender.End();
            inputDispatcher.End();
            inputReceiver.End();
            GameSystem.UpdateLoop.Hide(this);

            logicGameTime = null;
        }

        public void Update(GameTime gameTime)
        {
            timeAccumulation += gameTime.DeltaTime;

            // 프레임 재생 시간이 끝났고 새 입력이 있는 경우에만 새 로직 프레임 진행
            Network.Local.FrameInput input;
            while (timeAccumulation > LogicUpdateInterval
                && (input = inputReceiver.FetchInput()) != null)
            {
                // 플레이어 입력 동기화 처리
                for (int i = 0; i < session.MaxPlayer; i++)
                {
                    inputStates[i].ClearFrame();
                    List<InputCommand> list = input.Inputs[0].CommandList;
                    foreach (InputCommand cmd in list)
                    {
                        inputStates[i].ApplyCommand(cmd);
                    }
                }
                inputReceiver.NextInput();

                // 로직 업데이트 수행
                logicGameTime.Refresh();
                logicGameTime.SetDeltaTime(LogicUpdateInterval);
                combatLoop.UpdateLogic(logicGameTime);

                // 프레임 재생 시간 처리
                timeAccumulation -= LogicUpdateInterval;
            }

            // 프레임을 잔뜩 쌓아두지 않도록
            if (timeAccumulation > LogicUpdateInterval)
                timeAccumulation = 0;

            GameSystem.DebugOutput.SetWatch("Input Buffer Count", inputReceiver.BufferCount);
            combatLoop.Draw(gameTime, timeAccumulation / LogicUpdateInterval);
        }

        public PlayerInputState[] InputStates
        {
            get
            {
                return inputStates;
            }
        }

        #region privates

        GameTime logicGameTime;
        GameSession session;
        InputSender inputSender;
        InputReceiver inputReceiver;
        InputDispatcher inputDispatcher;
        PlayerInputState[] inputStates;
        float timeAccumulation = 0.0f;
        CombatLoop combatLoop;

        #endregion
    }
}

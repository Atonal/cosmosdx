﻿using SharpDX;

namespace HAJE.Cosmos.Combat.Rendering
{
    public class Camera
    {
        public const float Far = 100000.0f;
        public const float Near = 1.0f;

        public Camera(Viewport viewport)
        {
            FitViewTo(viewport);
            MoveTo(new Vector2(0, 0));
        }

        public void MoveTo(Vector2 position)
        {
            this.position = position;
            view = Matrix.Translation(
                -position.X,
                -position.Y,
                0
            );
            Matrix.Multiply(ref view, ref projection, out transform);
        }

        public void FitViewTo(Viewport viewport)
        {
            float fieldOfView = Radian.Pi / 4.0f;
            float screenAspect = (float)viewport.Width / viewport.Height;
            Matrix.PerspectiveFovLH(
                fieldOfView,
                screenAspect,
                Near,
                Far,
                out projection
            );
            Matrix.Multiply(ref view, ref projection, out transform);
        }

        public Matrix TransformMatrix
        {
            get
            {
                return transform;
            }
        }

        public Vector2 Position
        {
            get
            {
                return position;
            }
        }

        Matrix view;
        Matrix projection;
        Matrix transform;
        Vector2 position;
    }
}


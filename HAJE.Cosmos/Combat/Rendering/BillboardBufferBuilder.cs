﻿using HAJE.Cosmos.Rendering.VertexTypes;
using SharpDX;

namespace HAJE.Cosmos.Combat.Rendering
{
    public static class BillboardBuilder
    {
        public const int VerticesPerObject = 4;
        public const int IndicesPerObject = 6;
        public const int PremitivesPerObject = 2;

        public static int GetVertexBufferSize(int objectCount)
        {
            return GetVertexCount(objectCount) * PositionColorTextureVertexType.Size;
        }

        public static int GetVertexCount(int objectCount)
        {
            return objectCount * VerticesPerObject;
        }

        public static void WriteVertexBuffer(Color color, Vector4 position, DataStream buffer)
        {
            PositionColorTextureVertexType vertex;

            vertex.Color = color;
            vertex.Position = position;
            vertex.TextureUV = new Vector2(0, 0);
            vertex.WriteTo(buffer);

            vertex.TextureUV = new Vector2(0, 1);
            vertex.WriteTo(buffer);

            vertex.TextureUV = new Vector2(1, 0);
            vertex.WriteTo(buffer);

            vertex.TextureUV = new Vector2(1, 1);
            vertex.WriteTo(buffer);
        }

        public static void WriteVertexBuffer(RectangleF? uvSource, Color? tint, Vector4 position, Vector2 anchor, Vector2 size, Radian rotation, DataStream buffer)
        {
            PositionColorTextureVertexType vertex;
            RectangleF uv = uvSource ?? new RectangleF(0, 0, 1, 1);

            vertex.Color = tint ?? Color.White;

            Vector2 tl = new Vector2(-anchor.X * size.X, -anchor.Y * size.Y);
            Vector2 tr = new Vector2((1 - anchor.X) * size.X, -anchor.Y * size.Y);
            Vector2 bl = new Vector2(-anchor.X * size.X, (1 - anchor.Y) * size.Y);
            Vector2 br = new Vector2((1 - anchor.X) * size.X, (1 - anchor.Y) * size.Y);
            tl = rotation.Rotate(tl);
            tr = rotation.Rotate(tr);
            bl = rotation.Rotate(bl);
            br = rotation.Rotate(br);

            vertex.TextureUV = uv.TopLeft;
            vertex.Position = position;
            vertex.Position.X += tl.X;
            vertex.Position.Y += tl.Y;
            vertex.WriteTo(buffer);

            vertex.TextureUV = uv.BottomLeft;
            vertex.Position = position;
            vertex.Position.X += bl.X;
            vertex.Position.Y += bl.Y;
            vertex.WriteTo(buffer);

            vertex.TextureUV = uv.TopRight;
            vertex.Position = position;
            vertex.Position.X += tr.X;
            vertex.Position.Y += tr.Y;
            vertex.WriteTo(buffer);

            vertex.TextureUV = uv.BottomRight;
            vertex.Position = position;
            vertex.Position.X += br.X;
            vertex.Position.Y += br.Y;
            vertex.WriteTo(buffer);
        }

        public static int GetIndexBufferSize(int objectCount)
        {
            return GetIndexCount(objectCount) * sizeof(int);
        }

        public static int GetIndexCount(int objectCount)
        {
            return objectCount * IndicesPerObject;
        }

        public static void WriteIndexBuffer(int objectCount, DataStream buffer)
        {
            for (int i = 0; i < objectCount; i++)
            {
                buffer.Write(i * VerticesPerObject);
                buffer.Write(i * VerticesPerObject + 1);
                buffer.Write(i * VerticesPerObject + 2);
                buffer.Write(i * VerticesPerObject + 2);
                buffer.Write(i * VerticesPerObject + 1);
                buffer.Write(i * VerticesPerObject + 3);
            }
        }

        public static int GetPrimitiveCount(int objectCount)
        {
            return objectCount * PremitivesPerObject;
        }
    }
}

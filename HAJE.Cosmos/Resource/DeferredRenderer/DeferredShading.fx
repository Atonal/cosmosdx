﻿
struct VS_IN
{
    float4 pos : POSITION;
    float4 col : COLOR0;
    float2 tex : TEXCOORD0;
};

struct PS_IN
{
    float4 pos : POSITION;
    float4 col : COLOR0;
    float2 tex : TEXCOORD0;
};

float2 TexSize = { 2, 2 };
float3 Light = { 0, 0, 1 };
float3 Camera = { 0, 0, 4 };
static const float BumpUnit = 15;
static const float MaxDiffuse = 2;
static const float MaxAmbient = 2;
static const float MaxIntensity = 2;
static const float MaxShine = 8;

texture ColorTexture;
sampler ColorSampler = sampler_state {
    texture = <ColorTexture>;
    magfilter = LINEAR;
    minfilter = LINEAR;
    mipfilter = LINEAR;
    AddressU = clamp;
    AddressV = clamp;
};

texture DepthTexture;
sampler DepthSampler = sampler_state {
    texture = <DepthTexture>;
    magfilter = LINEAR;
    minfilter = LINEAR;
    mipfilter = LINEAR;
    AddressU = clamp;
    AddressV = clamp;
};

texture MaterialTexture;
sampler MaterialSampler = sampler_state {
    texture = <MaterialTexture>;
    magfilter = LINEAR;
    minfilter = LINEAR;
    mipfilter = LINEAR;
    AddressU = clamp;
    AddressV = clamp;
};

PS_IN VS(VS_IN input)
{
    return input;
}

float4 PS(PS_IN input) : COLOR
{
    float2 texCoord = input.tex;
    
    // depth to normal
    float unitU = 1.0f / TexSize.x;
    float unitV = 1.0f / TexSize.y;
    float4 center = tex2D(DepthSampler, texCoord + float2(0, 0));
    float4 left = tex2D(DepthSampler, texCoord + float2(-unitU, 0));
    float4 right = tex2D(DepthSampler, texCoord + float2(unitU, 0));
    float4 top = tex2D(DepthSampler, texCoord + float2(0, -unitV));
    float4 bottom = tex2D(DepthSampler, texCoord + float2(0, unitV));
    if (left.a < 1) left.r = center.r;
    if (right.a < 1) right.r = center.r;
    if (top.a < 1) top.r = center.r;
    if (bottom.a < 1) bottom.r = center.r;
    float3 dx = { 2, 0, (right.r - left.r) * input.col.r * 256 / BumpUnit };
    float3 dy = { 0, 2, (bottom.r - top.r) * input.col.r * 256 / BumpUnit };
    float3 normal = normalize(cross(dx, dy));


    float4 material = tex2D(MaterialSampler, texCoord);

    // ambient
    float ambient = (1 - material.a);

    // diffuse
    float lightAmount = max(dot(normal, Light), 0);
    float diffuse = lightAmount * material.r * MaxDiffuse;

    // specular
    float3 reflect = normalize(2 * dot(normal, Light) * normal - Light);
    float3 view = normalize(Camera - float3(texCoord.x - 0.5, texCoord.y - 0.5, 0) * 2);

    lightAmount = max(dot(view, reflect), 0);
    float intensity = material.g * MaxIntensity;
    float shine = material.b * MaxShine;
    float specular = pow(lightAmount, shine) * intensity;

    // occulusion
    float occulusion = max(left.r - center.r, 0) + max(top.r - center.r, 0);

    // final blending
    float4 textureColor = tex2D(ColorSampler, texCoord);
    float4 vertexColor = input.col;

    float4 result = (textureColor * (ambient + diffuse) + specular * vertexColor) * (1 - occulusion/2);
    result.a = textureColor.a * vertexColor.a;
    return result;
}

technique Main {
    pass P0 {
        VertexShader = compile vs_2_0 VS();
        PixelShader = compile ps_2_0 PS();
    }
}

﻿using HAJE.Cosmos.Combat.Background;
using SharpDX;

namespace HAJE.Cosmos.Combat
{
    public class SampleBackground : IBackgroundBuilder
    {
        public void Build(BackgroundRenderer renderer)
        {
            var starBuilder = new SimpleStarBuilder();
            starBuilder.InitSeed(0);
            starBuilder.AddPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.125f, 0.25f),
                ColorFrom = StarColor.White.Color3,
                ColorTo = StarColor.White.Color3,
                ToBlend = new FloatRange(0, 1),
            }, 1);
            starBuilder.AddUniformRectangular(-20000, 20000, -20000, 20000, 9000, 11000, 0.15f);
            renderer.SimpleStarRenderer.SetDotStar(starBuilder.FlushData());

            starBuilder.AddPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.125f, 1.0f),
                ColorFrom = StarColor.Red.Color3,
                ColorTo = StarColor.Orange.Color3,
                ToBlend = new FloatRange(0, 1),
            }, 1);
            starBuilder.AddUniformRectangular(-20000, 20000, -20000, 20000, 7500, 11000, 0.05f);
            renderer.SimpleStarRenderer.SetSpriteStar(starBuilder.FlushData());

            var flareBuilder = new Background.FlareStarBuilder();
            flareBuilder.AddAnimationGroupPalette(0);
            flareBuilder.AddAnimationGroupPalette(1);
            flareBuilder.AddAnimationGroupPalette(2);
            flareBuilder.AddColorToPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.5f, 1),
                ColorFrom = StarColor.Red.Color3,
                ColorTo = StarColor.Orange.Color3,
                ToBlend = new FloatRange(0, 1),
            }, 1);
            flareBuilder.SetScaleRange(2.0f, 4.0f);
            flareBuilder.SetFlareScaleVariation(0.33f, 1.15f);
            flareBuilder.AppendUniform(
                new Vector3(-20000, -20000, 9000),
                new Vector3(20000, 20000, 11000),
                250, 0
            );
            flareBuilder.SetScaleRange(3.0f, 5.0f);
            flareBuilder.SetFlareScaleVariation(0.75f, 1.15f);
            flareBuilder.AppendUniform(
                new Vector3(-20000, -20000, 9000),
                new Vector3(20000, 20000, 11000),
                100, 1
            );
            flareBuilder.SetScaleRange(1.5f, 3.0f);
            flareBuilder.AppendCircular(
                new Vector3(1000.0f, 250.0f, 10000.0f),
                new Vector3(2000.0f, 300.0f, 3000.0f),
                new Vector3(0.4f, 0.2f, 0.0f),
                20, 0
            );
            flareBuilder.SetScaleRange(3.0f, 8.0f);
            flareBuilder.AppendCircular(
                new Vector3(1000.0f, 250.0f, 10000.0f),
                new Vector3(2000.0f, 300.0f, 3000.0f),
                new Vector3(0.4f, 0.2f, 0.0f),
                5, 1
            );
            flareBuilder.Append(new FlareStarData()
            {
                AnimationGroup = 3,
                Color = flareBuilder.GetPaletteColor(1),
                FlareRadius = 36.0f,
                LightSourceRadius = 24.0f,
                Position = new Vector3(1250.0f, 128.0f, 3000.0f)
            });
            renderer.FlareStarRenderer.SetData(flareBuilder.ToArray());

            for (int i = 0; i < 4; i++)
            {
                renderer.FlareStarRenderer.GetAnimation(i).SetAngularAnimation((Radian)0.5f, 0);
            }
            renderer.FlareStarRenderer.GetAnimation(0).SetScaleAnimation(0.70f, 1.00f, 1.15f, 0.00f);
            renderer.FlareStarRenderer.GetAnimation(1).SetScaleAnimation(0.70f, 1.00f, 1.15f, 0.75f);
            renderer.FlareStarRenderer.GetAnimation(2).SetScaleAnimation(0.70f, 1.00f, 1.00f, 0.35f);
            renderer.FlareStarRenderer.GetAnimation(3).SetScaleAnimation(0.85f, 1.00f, 2.00f, 0.35f);

            Background.NebularBuilder nebulaBuilder = new Background.NebularBuilder();
            nebulaBuilder.AddColorToPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.5f, 0.75f),
                ColorFrom = StarColor.Red.Color3,
                ColorTo = StarColor.Orange.Color3,
                ToBlend = new FloatRange(0, 1),
            }, 1);
            nebulaBuilder.AddColorToPalette(new ColorPalette()
            {
                Brightness = new FloatRange(0.5f, 0.75f),
                ColorFrom = StarColor.Red.Color3,
                ColorTo = StarColor.Blue.Color3,
                ToBlend = new FloatRange(0, 0.88f),
            }, 1);
            nebulaBuilder.AppendUniform(
                new Vector3(-20000, -20000, 9000),
                new Vector3(20000, 20000, 11000),
                150, 0
            );
            renderer.NebulaRenderer.BuildMesh(nebulaBuilder.ToArray());

            Background.DoodadBuilder doodadBuilder = new Background.DoodadBuilder();
            doodadBuilder.AddDoodad(new DoodadData()
                {
                    Position = new Vector3(-200, -150, 5000),
                    Scale = 2.0f,
                    Texture = "Resource/Background/Uranus.png",
                    Layer = DoodadLayer.Back
                });
            doodadBuilder.AddDoodad(new DoodadData()
                {
                    Position = new Vector3(1000, -800, 3000),
                    Scale = 1.5f,
                    Texture = "Resource/Background/Desert.png",
                    Layer = DoodadLayer.Back
                });
            doodadBuilder.AddDoodad(new DoodadData()
                {
                    Position = new Vector3(-1000, 500, 2500),
                    Scale = 1.4f,
                    Texture = "Resource/Background/Jupiter.png",
                    Layer = DoodadLayer.Back
                });
            doodadBuilder.AddDoodad(new DoodadData()
                {
                    Position = new Vector3(800, 100, 1250),
                    Scale = 0.8f,
                    Texture = "Resource/Background/Mars.png",
                    Layer = DoodadLayer.Back
                });
            doodadBuilder.SetTextureSet(
                "Resource/Background/Asteroid0.png",
                "Resource/Background/Asteroid1.png",
                "Resource/Background/Asteroid2.png",
                "Resource/Background/Asteroid3.png"
            );
            doodadBuilder.SetSeed(0);
            doodadBuilder.SetScaleRange(0.25f, 0.5f);
            doodadBuilder.SetRandomRotation(true);
            doodadBuilder.AddCircular(new Vector3(-150, -250, 800), new Vector3(150, 400, 150), new Vector3(0, 0, 0), DoodadLayer.Middle, 25);
            doodadBuilder.AddCircular(new Vector3(150, 250, 800), new Vector3(150, 400, 150), new Vector3(0, 0, -0.5f), DoodadLayer.Middle, 50);
            doodadBuilder.AddCircular(new Vector3(150, -750, 800), new Vector3(150, 400, 150), new Vector3(0, 0, 0.5f), DoodadLayer.Middle, 25);
            renderer.DoodadRenderer.BuildMesh(doodadBuilder.FlushData());
        }
    }
}

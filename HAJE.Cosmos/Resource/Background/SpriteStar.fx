﻿
struct VS_IN
{
    float4 pos : POSITION;
    float4 col : COLOR0;
    float2 tex : TEXCOORD0;
};

struct PS_IN
{
    float4 pos : POSITION;
    float4 col : COLOR0;
    float2 tex : TEXCOORD0;
};

float4x4 transform;
float4 viewportSize;

PS_IN VS( VS_IN input )
{
    PS_IN output;
    output.pos = mul(input.pos, transform);
    output.pos.x /= output.pos.w;
    output.pos.y /= output.pos.w;
    output.pos.z = 0;
    output.pos.w = 1;
    output.pos.x += input.tex.x * input.pos.w / viewportSize.x * 2 * 4;
    output.pos.y += input.tex.y * input.pos.w / viewportSize.y * 2 * 4;
    output.col = input.col;
    output.tex = input.tex;
    return output;
}

float4 PS( PS_IN input ) : COLOR
{
    float i = 1;
    float3 c = input.col;
    float x = input.tex.x;
    float y = input.tex.y;
    x = (x - 0.5f) * 2;
    y = (y - 0.5f) * 2;
    i = 1 - sqrt(x * x + y * y);
    if(i > 0.66f)
    {
        float a = (i - 0.66f) / 0.66f;
        c += float3(a, a, a);
    }
    return float4(c * i, 1);
}

technique Main {
	pass P0 {
		VertexShader = compile vs_2_0 VS();
        PixelShader  = compile ps_2_0 PS();
	}
}

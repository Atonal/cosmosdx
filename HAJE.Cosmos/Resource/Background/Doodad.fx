﻿
struct VS_IN
{
    float4 pos : POSITION;
    float4 col : COLOR0;
    float2 tex : TEXCOORD0;
};

struct PS_IN
{
    float4 pos : POSITION;
    float4 col : COLOR0;
    float2 tex : TEXCOORD0;
};

sampler2D tex;
float4x4 transform;
float2 textureSize;

PS_IN VS( VS_IN input )
{
    PS_IN output;
    output.pos = mul(input.pos, transform);
    output.pos.x /= output.pos.w;
    output.pos.y /= output.pos.w;
    output.pos.z = 0;
    output.pos.w = 1;
    output.col = input.col;
    output.tex = input.tex;
    return output;
}

float4 PS( PS_IN input ) : COLOR
{
    float brightness = input.col.r;
    float sharpness = input.col.g;
    float saturation = input.col.b;

    float dx = 1 / textureSize.x;
    float dy = 1 / textureSize.y;
    float4 sum = 0;
    sum += tex2D(tex, input.tex + float2(-dx, -dy));
    sum += tex2D(tex, input.tex + float2(0, -dy));
    sum += tex2D(tex, input.tex + float2(dx, -dy));
    sum += tex2D(tex, input.tex + float2(-dx, 0));
    sum += tex2D(tex, input.tex + float2(0, 0));
    sum += tex2D(tex, input.tex + float2(dx, 0));
    sum += tex2D(tex, input.tex + float2(-dx, dy));
    sum += tex2D(tex, input.tex + float2(0, dy));
    sum += tex2D(tex, input.tex + float2(dx, dy));
    sum += tex2D(tex, input.tex + float2(-dx * 2, 0));
    sum += tex2D(tex, input.tex + float2(dx * 2, 0));
    sum += tex2D(tex, input.tex + float2(0, dy * 2));
    sum += tex2D(tex, input.tex + float2(0, -dy * 2));
    float4 blur = sum / 12 * (1 - sharpness) + tex2D(tex, input.tex) * sharpness;
    float3 color = blur.rgb;
    float3 luminanceWeights = float3(0.299, 0.587, 0.114);
    float luminance = dot(color, luminanceWeights);
    color = lerp(luminance, color, saturation);
    return float4(color * brightness, blur.a);
}

technique Main {
	pass P0 {
		VertexShader = compile vs_2_0 VS();
        PixelShader  = compile ps_2_0 PS();
	}
}
